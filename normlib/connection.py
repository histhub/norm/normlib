from typing import TYPE_CHECKING

from eavlib.connection import EAVConnection
from eavlib.db.url import (URL, make_url)
from eavlib.graph import Graph

from normlib.db.sidecar_db import SidecarDBConnection

if TYPE_CHECKING:
    from typing import Union

    from normlib.db.dump_manager import DumpManager
    from normlib.db.entity_manager import EntityManager
    from normlib.db.id_manager import HistHubIDManager
    from normlib.db.log_manager import LogManager
    from normlib.db.revision_manager import RevisionManager
    from normlib.db.sequence_manager import SequenceManager


class NormDBConnection:
    __slots__ = ("graph", "sidecar_db")

    def __init__(self, graph: "Graph",
                 sidecar_db: "SidecarDBConnection") -> None:
        self.graph = graph
        self.sidecar_db = sidecar_db

    @classmethod
    def fromstring(cls, url: "Union[URL, str]",
                   data_schema: str = "data",
                   sidecar_schema: str = "sidecar",
                   model: "Union[int, str]" = "histHub") -> "NormDBConnection":
        if not isinstance(url, URL):
            url = make_url(url)

        graph_url = url.copy()
        graph_url.schema = data_schema

        sidecar_url = url.copy()
        sidecar_url.schema = sidecar_schema

        eav_conn = EAVConnection(graph_url)  # type: ignore
        graph = Graph(eav_conn, model)
        sidecar_db = SidecarDBConnection(sidecar_url)

        return NormDBConnection(graph, sidecar_db)

    def copy(self) -> "NormDBConnection":
        new_graph = self.graph.copy()
        new_sidecar_db = self.sidecar_db.copy()
        return NormDBConnection(new_graph, new_sidecar_db)

    def dump_manager(self) -> "DumpManager":
        from normlib.db.dump_manager import DumpManager
        return DumpManager(self)  # type: ignore

    def entity_manager(self) -> "EntityManager":
        from normlib.db.entity_manager import EntityManager
        return EntityManager(self)  # type: ignore

    def id_manager(self) -> "HistHubIDManager":
        from normlib.db.id_manager import HistHubIDManager
        return HistHubIDManager(self.sidecar_db)  # type: ignore

    def log_manager(self) -> "LogManager":
        from normlib.db.log_manager import LogManager
        return LogManager(self.sidecar_db)  # type: ignore

    def revision_manager(self) -> "RevisionManager":
        from normlib.db.revision_manager import RevisionManager
        return RevisionManager(self.sidecar_db)  # type: ignore

    def sequence_manager(self) -> "SequenceManager":
        from normlib.db.sequence_manager import SequenceManager
        return SequenceManager(self.sidecar_db)  # type: ignore
