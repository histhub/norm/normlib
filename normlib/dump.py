import xml.etree.ElementTree

from collections import namedtuple
from typing import (Any, Callable, Dict)
from xml.dom import minidom
from xml.sax.saxutils import escape as xml_escape

DeepNode = namedtuple("DeepNode", ("node", "label", "attributes", "relations"))
VirtualDeepNode = namedtuple("VirtualDeepNode", ("node", "label"))


def urn_formatter(node_id: int) -> str:
    # XXX: URN for internal IDs? A good idea?
    return "urn:histhub:%u" % node_id


class DeepNodeSerialiser:
    @classmethod
    def as_dict(cls, deep_node: DeepNode, enable_hidden: bool = False,
                href_formatter: "Callable[[int], str]" = urn_formatter) \
            -> Dict[str, Any]:
        hidden_attributes = {}

        print(enable_hidden)
        if enable_hidden:
            hidden_attributes.update({
                "@id": deep_node.node.id,
                "@label": deep_node.label,
                })

        if isinstance(deep_node, VirtualDeepNode):
            if enable_hidden:
                hidden_attributes["@href"] = href_formatter(deep_node.node.id)
            return hidden_attributes

        return {
            **hidden_attributes,
            **deep_node.attributes,
            **{
                k: [cls.as_dict(n, enable_hidden, href_formatter) for n in v]
                if isinstance(v, list)
                else cls.as_dict(v, enable_hidden, href_formatter)
                for k, v in deep_node.relations.items()
                }
            }

    @classmethod
    def as_xml(cls, deep_node: DeepNode, enable_hidden: bool = False,
               pretty: bool = False,
               href_formatter: "Callable[[int], str]" = urn_formatter) \
            -> str:
        doc = minidom.getDOMImplementation().createDocument(None, None, None)

        def _toxml(deep_node: DeepNode) -> minidom.Element:
            xml_elem = doc.createElement(deep_node.label)
            if enable_hidden:
                xml_elem.setAttribute("_id", str(deep_node.node.id))

            if isinstance(deep_node, VirtualDeepNode):
                if enable_hidden:
                    xml_elem.setAttribute(
                        "_href", href_formatter(deep_node.node.id))
            else:
                for attr, value in deep_node.attributes.items():
                    # NOTE: Escape invalid XML characters (cf.
                    #       https://www.w3.org/TR/xml/#charsets)
                    # NOTE: 0x[9AD] would be allowed, but they make the XML
                    #       "harder" to read.
                    xml_elem.setAttribute(attr, xml_escape(value, {
                        "%c" % x: "&#x%X;" % x
                        for x in range(1, 0x20)
                        # if x not in (0x9, 0xA, 0xD)
                        }))

                for rel in sorted(deep_node.relations):
                    rel_elem = doc.createElement(rel)

                    nodes = deep_node.relations[rel]
                    if not isinstance(nodes, list):
                        nodes = [nodes]
                    for n in sorted(nodes, key=lambda n: int(n.node.id)):
                        rel_elem.appendChild(_toxml(n))
                    xml_elem.appendChild(rel_elem)

            return xml_elem

        doc.appendChild(_toxml(deep_node))
        if pretty:
            xml_bytes = doc.toprettyxml(encoding="utf-8")
        else:
            xml_bytes = doc.toxml(encoding="utf-8")

        xmlstr = str(xml_bytes.decode())

        if hasattr(xml.etree.ElementTree, "canonicalize"):
            # NOTE: Python >= 3.8 only. On older versions XML output is sorted
            #       implicitly.
            xmlstr = xml.etree.ElementTree.canonicalize(xmlstr)  # type: ignore

        return xmlstr
