import argparse

from cliff.command import Command  # type: ignore

from eavlib.db.url import make_url
from normlib.db.sidecar_db import SidecarDBConnection
from normlib.db.sequence_manager import SequenceManager

HHB_ID_SEQUENCE_NAME = "histhub_id_seq"


class GetHHbID(Command):  # type: ignore
    """Get the current value of the histHub ID sequence."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument("url", help="sidecar schema connection URL")

        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> int:
        sidecar_db = SidecarDBConnection(make_url(parsed_args.url))
        sequence_manager = SequenceManager(  # type: ignore
            sidecar_db, HHB_ID_SEQUENCE_NAME, debug=False)

        print(sequence_manager.current_value())
        return 0


class SetHHbID(Command):  # type: ignore
    """Set the value of the histHub ID sequence."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument("url", help="sidecar schema connection URL")

        parser.add_argument("--force", action="store_true")
        parser.add_argument("value", type=int)

        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> int:
        sidecar_db = SidecarDBConnection(make_url(parsed_args.url))
        sequence_manager = SequenceManager(  # type: ignore
            sidecar_db, HHB_ID_SEQUENCE_NAME, debug=False)

        sequence_manager.set_value(parsed_args.value, force=parsed_args.force)
        return 0
