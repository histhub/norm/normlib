import argparse
import logging

from cliff.command import Command  # type: ignore
from typing import TYPE_CHECKING

from normlib import HistHubID
from normlib.connection import NormDBConnection

if TYPE_CHECKING:
    from typing import (Iterable, Iterator)

LOG = logging.getLogger(__name__)


class TakeSnapshot(Command):  # type: ignore
    """Take a snapshot of the histHub database in a revision."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument("url", help="DB connection URL (no schema)")

        parser.add_argument(
            "--data-schema", default="data",
            help="name of the schema to contain the (eavlib-compatible) data")
        parser.add_argument(
            "--model", default="histHub",
            help="name of the ealib graph model")
        parser.add_argument(
            "--sidecar-schema", default="sidecar",
            help="name of the schema to contain sidecar information")

        parser.add_argument(
            "-m", "--message", required=True,
            help="the message for the new revision")

        return parser

    def _dirty(self, hhb_ids: "Iterable[HistHubID]") -> "Iterator[HistHubID]":
        def _is_dirty(hhb_id: "HistHubID") -> bool:
            return next(self.entity_manager.diff(hhb_id), None) is not None
        return filter(_is_dirty, hhb_ids)

    def take_action(self, parsed_args: argparse.Namespace) -> int:
        norm_db = NormDBConnection.fromstring(
            parsed_args.url, parsed_args.data_schema,
            parsed_args.sidecar_schema, parsed_args.model)

        if not parsed_args.message:
            raise ValueError("no --message given")

        self.dump_manager = norm_db.dump_manager()
        self.entity_manager = norm_db.entity_manager()
        self.revision_manager = norm_db.revision_manager()

        try:
            LOG.info("Scanning DB...")
            dirty_objects = self._dirty(self.entity_manager.all_entities())
            hhb_id = next(dirty_objects)

            # NOTE: only create revision if there are any dirty objects
            message = parsed_args.message
            rev = self.revision_manager.create_revision(message)

            while hhb_id:
                LOG.info(
                    "Taking snapshot of entity with histHub ID: %u", hhb_id)
                self.dump_manager.snapshot_entity(hhb_id)
                try:
                    hhb_id = next(dirty_objects)
                except StopIteration:
                    break

            # Commit revision and write snapshots to DB
            self.revision_manager.commit_revision(rev)
        except StopIteration:
            # status is clean
            LOG.info("State of DB is clean.")
        except Exception:
            if locals().get("rev", None) is not None:
                LOG.warning("Rolling back revision r%u", rev.id)
                self.revision_manager.rollback_revision(rev)

        return 0
