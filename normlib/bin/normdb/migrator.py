import argparse
import curses
import logging

from cliff.command import Command  # type: ignore
from eavlib.connection import EAVConnection
from eavlib.core.storage_manager import StorageManager
from eavlib.db.url import make_url
from typing import TYPE_CHECKING

from normlib.db.migrator import SidecarDBMigrator

LOG = logging.getLogger(__name__)

curses.setupterm()
_has_colour_term = curses.tigetnum("colors") > 1

if TYPE_CHECKING:
    from eavlib.db.url import URL
    from typing import Union


class DebugHooks:
    def before_migration(version: int) -> None:  # type: ignore
        print("=> schema migration to version %u" % (version))

    def after_migration(  # type: ignore
            version: int, num_scripts_executed: int) -> None:
        msg = "OK"
        if _has_colour_term:
            print("\r\033[32m%s\033[0m." % (msg))
        else:
            print("%s." % (msg))

    def failed_migration(  # type: ignore
            version: int, e: BaseException) -> None:
        msg = "FAIL"
        if _has_colour_term:
            print("\r\033[41m%s\033[0m." % (msg))
        else:
            print("%s." % (msg))

        # e will be reraised in migrator

    def log_message(level: int, msg: str) -> None:  # type: ignore
        if msg.endswith("\n"):
            msg = msg[:-1]
        LOG.log(level, msg)


class TermHooks:
    def before_migration(version: int) -> None:  # type: ignore
        print("[....]", "schema migration to version %u" % (version),
              end="", flush=True)

    def after_migration(  # type: ignore
            version: int, num_scripts_executed: int) -> None:
        msg = "OK"
        if _has_colour_term:
            print("\r\033[32m[{:^4}]\033[0m".format(msg))
        else:
            print("\r[{:^4}]".format(msg))

    def failed_migration(  # type: ignore
            version: int, e: BaseException) -> None:
        msg = "FAIL"
        if _has_colour_term:
            print("\r\033[41m[{:^4}]\033[0m".format(msg))
        else:
            print("\r[{:^4}]".format(msg))

        # e will be reraised in migrator


class DBManager:
    def __init__(self, url: "Union[str, URL]",
                 data_schema_name: str, sidecar_schema_name: str,
                 debug: bool = False) -> None:
        url = make_url(url)

        if url.schema:
            raise ValueError("The URL must not contain a schema name")

        # Data schema manager
        data_url = url.copy()
        data_url.schema = data_schema_name
        eav_conn = EAVConnection(data_url)  # type: ignore
        self.storage_manager = StorageManager(eav_conn, debug=debug)

        # Sidecar schema manager
        sidecar_url = url.copy()
        sidecar_url.schema = sidecar_schema_name
        self.sidecar_migrator = SidecarDBMigrator(  # type: ignore
            sidecar_url, data_schema_name, debug=debug)

        hooks_provider = TermHooks if not debug else DebugHooks
        hooks = {name: func for name, func in vars(hooks_provider).items()
                 if callable(func) and not name.startswith("__")}
        self.storage_manager.migrator.register_hooks(**hooks)
        self.sidecar_migrator.register_hooks(**hooks)

    def format(self) -> None:
        actions_taken = False

        # Data schema
        if self.storage_manager.schema_exists():
            LOG.info("Data schema already exists.")
        else:
            # Create data schema
            LOG.info("Creating data schema…")
            self.storage_manager.create_schema()

        if not self.storage_manager.is_created():
            LOG.info("Formatting data schema…")
            self.storage_manager.create()
            actions_taken = True

        # Sidecar schema
        if self.sidecar_migrator.schema_exists():
            LOG.info("Sidecar schema already exists.")
        else:
            # Create sidecar schema
            LOG.info("Creating sidecar schema…")
            self.sidecar_migrator.create_schema()

        if not self.sidecar_migrator.is_created():
            LOG.info("Formatting sidecar schema…")
            self.sidecar_migrator.create()
            actions_taken = True

        if not actions_taken:
            raise RuntimeError("Database is already formatted.")

    def migrate(self) -> None:
        if not self.storage_manager.is_created():
            raise RuntimeError("Data schema is not formatted")
        if not self.sidecar_migrator.is_created():
            raise RuntimeError("Sidecar schema is not formatted")

        # Migrate data schema to target schema version
        data_version = StorageManager._latest_version
        if self.storage_manager.migration_needed(data_version):
            LOG.info("Migrating data schema to version %u...", data_version)
            self.storage_manager.migrate(data_version)
        else:
            LOG.info("Data schema is already at version %u. Nothing to do.",
                     self.storage_manager.migrator.current_version())

        # Migrate sidecar schema to target schema version
        sidecar_version = SidecarDBMigrator._latest_version
        if self.sidecar_migrator.migration_needed(sidecar_version):
            LOG.info("Migrating sidecar schema to version %u...",
                     sidecar_version)
            self.sidecar_migrator.migrate(sidecar_version)
        else:
            LOG.info("Sidecar schema is already at version %u. Nothing to do.",
                     self.sidecar_migrator.current_version())


class FormatDB(Command):  # type: ignore
    """Format a new Norm DB."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument("url", help="database connection URL")
        parser.add_argument(
            "--show-logs", required=False, action="store_true",
            help="Print messages useful for debugging.")

        parser.add_argument(
            "--data-schema", default="data",
            help="name of the schema to contain the (eavlib-compatible) data")
        parser.add_argument(
            "--sidecar-schema", default="sidecar",
            help="name of the schema to contain sidecar information")

        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> int:
        db_manager = DBManager(
            url=parsed_args.url,
            data_schema_name=parsed_args.data_schema,
            sidecar_schema_name=parsed_args.sidecar_schema,
            debug=parsed_args.show_logs)

        db_manager.format()
        db_manager.migrate()

        return 0


class MigrateDB(Command):  # type: ignore
    """Migrate an existing Norm DB to the latest schema revision."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument("url", help="database connection URL")
        parser.add_argument(
            "--show-logs", required=False, action="store_true",
            help="Print messages useful for debugging.")

        parser.add_argument(
            "--data-schema", default="data",
            help="name of the schema to contain the (eavlib-compatible) data")
        parser.add_argument(
            "--sidecar-schema", default="sidecar",
            help="name of the schema to contain sidecar information")

        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> int:
        db_manager = DBManager(
            url=parsed_args.url,
            data_schema_name=parsed_args.data_schema,
            sidecar_schema_name=parsed_args.sidecar_schema,
            debug=parsed_args.show_logs)

        db_manager.migrate()

        return 0
