import argparse

from cliff.command import Command  # type: ignore

from normlib import HistHubID
from normlib.connection import NormDBConnection
from normlib.dump import DeepNodeSerialiser


class DumpHHbEntity(Command):  # type: ignore
    """Dump an entity in the histHub database."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument("--url", help="DB connection URL (no schema)")

        parser.add_argument(
            "--data-schema", default="data",
            help="name of the schema to contain the (eavlib-compatible) data")
        parser.add_argument(
            "--model", default="histHub",
            help="name of the ealib graph model")
        parser.add_argument(
            "--sidecar-schema", default="sidecar",
            help="name of the schema to contain sidecar information")

        parser.add_argument(
            "hhb_id", type=HistHubID,
            help="the histHub ID of the entity to be dumped")

        return parser

    def take_action(self, parsed_args: argparse.Namespace) -> int:
        hhb_id = parsed_args.hhb_id
        norm_db = NormDBConnection.fromstring(
            parsed_args.url, parsed_args.data_schema,
            parsed_args.sidecar_schema, parsed_args.model)

        dump_manager = norm_db.dump_manager()
        dn = dump_manager.dump_entity(hhb_id)
        if not dn:
            raise ValueError("No entity with histHub ID: %u" % hhb_id)

        print(DeepNodeSerialiser.as_xml(dn, enable_hidden=True, pretty=True))

        return 0
