import argparse

from cliff.command import Command  # type: ignore
from cliff.lister import Lister  # type: ignore

from normlib.connection import NormDBConnection
from normlib.db.qa import (NormDBAnalyser, NormDBQA)

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from normlib.db.qa import _Tabular


def qa_get_parser(parser: argparse.ArgumentParser) -> argparse.ArgumentParser:
    parser.add_argument("url", help="sidecar schema connection URL")

    parser.add_argument(
        "--data-schema", default="data",
        help="name of the schema to contain the (eavlib-compatible) data")
    parser.add_argument(
        "--model", default="histHub",
        help="name of the ealib graph model")
    parser.add_argument(
        "--sidecar-schema", default="sidecar",
        help="name of the schema to contain sidecar information")

    return parser


class QAAll(Command):  # type: ignore
    """Run all QA checkers."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser

        parser.add_argument(
            "-j", type=int, default="4", dest="jobs",
            help="number of checks to execute in parallel, defaults to 4"
        )

        return qa_get_parser(parser)

    def take_action(self, parsed_args: argparse.Namespace) -> int:
        rc = 0

        norm_db = NormDBConnection.fromstring(
            parsed_args.url, parsed_args.data_schema,
            parsed_args.sidecar_schema, parsed_args.model)
        qa = NormDBQA(norm_db)  # type: ignore

        res = qa.check_all(mp=parsed_args.jobs)

        for c, errors in res.items():
            print("%s: found %u errors." % (c, len(errors)))
            if errors:
                rc = 1

        return rc


class QAMissingRequiredAttributes(Lister):  # type: ignore
    """Check for missing required attributes."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser
        return qa_get_parser(parser)

    def take_action(self, parsed_args: argparse.Namespace) -> "_Tabular":
        norm_db = NormDBConnection.fromstring(
            parsed_args.url, parsed_args.data_schema,
            parsed_args.sidecar_schema, parsed_args.model)
        qa = NormDBQA(norm_db)  # type: ignore

        return qa.check(NormDBQA.Checkers.missing_attributes)


class QARelationCardinalities(Lister):  # type: ignore
    """Check for incorrect object relation cardinalities."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser
        return qa_get_parser(parser)

    def take_action(self, parsed_args: argparse.Namespace) -> "_Tabular":
        norm_db = NormDBConnection.fromstring(
            parsed_args.url, parsed_args.data_schema,
            parsed_args.sidecar_schema, parsed_args.model)
        qa = NormDBQA(norm_db)  # type: ignore

        return qa.check(NormDBQA.Checkers.relation_cardinalities)


class QAIllegalAttributes(Lister):  # type: ignore
    """Check for illegal attributes."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser
        return qa_get_parser(parser)

    def take_action(self, parsed_args: argparse.Namespace) -> "_Tabular":
        norm_db = NormDBConnection.fromstring(
            parsed_args.url, parsed_args.data_schema,
            parsed_args.sidecar_schema, parsed_args.model)
        qa = NormDBQA(norm_db)  # type: ignore

        return qa.check(NormDBQA.Checkers.illegal_attributes)


class QAIllegalRelations(Lister):  # type: ignore
    """Check for illegal relations."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser
        return qa_get_parser(parser)

    def take_action(self, parsed_args: argparse.Namespace) -> "_Tabular":
        norm_db = NormDBConnection.fromstring(
            parsed_args.url, parsed_args.data_schema,
            parsed_args.sidecar_schema, parsed_args.model)
        qa = NormDBQA(norm_db)  # type: ignore

        return qa.check(NormDBQA.Checkers.illegal_relations)


class QADuplicateExternalIDs(Lister):  # type: ignore
    """Check for duplicate external IDs."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser
        return qa_get_parser(parser)

    def take_action(self, parsed_args: argparse.Namespace) -> "_Tabular":
        norm_db = NormDBConnection.fromstring(
            parsed_args.url, parsed_args.data_schema,
            parsed_args.sidecar_schema, parsed_args.model)
        qa = NormDBQA(norm_db)  # type: ignore

        return qa.check(NormDBQA.Checkers.external_ids)


class QAOwnership(Lister):  # type: ignore
    """Check if owners are providers."""

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser
        return qa_get_parser(parser)

    def take_action(self, parsed_args: argparse.Namespace) -> "_Tabular":
        norm_db = NormDBConnection.fromstring(
            parsed_args.url, parsed_args.data_schema,
            parsed_args.sidecar_schema, parsed_args.model)
        qa = NormDBQA(norm_db)  # type: ignore

        return qa.check(NormDBQA.Checkers.ownership)


class QAZombies(Lister):  # type: ignore
    """Check for zombie objects.

    Only identifiable objects are allows "free-standing"
    (without incoming relations)
    """

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser
        return qa_get_parser(parser)

    def take_action(self, parsed_args: argparse.Namespace) -> "_Tabular":
        norm_db = NormDBConnection.fromstring(
            parsed_args.url, parsed_args.data_schema,
            parsed_args.sidecar_schema, parsed_args.model)
        qa = NormDBQA(norm_db)  # type: ignore

        return qa.check(NormDBQA.Checkers.zombies)


class QARevisionLogIntegrity(Lister):  # type: ignore
    """Check the integrity of the revision log.

    This check checks if all identifiable objects have an entry in the revision
    log and the corresponding latest dump matches the state in the DB.
    """

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser
        return qa_get_parser(parser)

    def take_action(self, parsed_args: argparse.Namespace) -> "_Tabular":
        norm_db = NormDBConnection.fromstring(
            parsed_args.url, parsed_args.data_schema,
            parsed_args.sidecar_schema, parsed_args.model)
        qa = NormDBQA(norm_db)  # type: ignore

        return qa.check(NormDBQA.Checkers.revlog_integrity)


class QADataAnalyser(Command):  # type: ignore
    """Analyse the data in the database.

    This check analyses the data in the database to find potential issues with
    the data.
    """

    def get_parser(self, prog_name: str) -> argparse.ArgumentParser:
        parser = super().get_parser(prog_name)  # type: argparse.ArgumentParser
        return qa_get_parser(parser)

    def take_action(self, parsed_args: argparse.Namespace) -> int:
        norm_db = NormDBConnection.fromstring(
            parsed_args.url, parsed_args.data_schema,
            parsed_args.sidecar_schema, parsed_args.model)
        analyser = NormDBAnalyser(norm_db)

        return sum(analyser.analyse().values())
