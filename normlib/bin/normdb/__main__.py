if __name__ == "__main__":
    import re
    import sys
    from .main import main

    sys.argv[0] = re.sub(r"^.*\.", "", __package__)
    sys.exit(main())
