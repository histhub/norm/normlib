#!/usr/bin/env python3

import argparse
import configparser
import importlib
import logging
import sys

from cliff.app import App  # type: ignore
from cliff.command import Command  # type: ignore
from cliff.commandmanager import (  # type: ignore
    EntryPointWrapper, CommandManager)
from typing import (Dict, List, Optional, NoReturn, TYPE_CHECKING)

from normlib import __version__
from normlib.bin.normdb import __description__

if TYPE_CHECKING:
    import pkg_resources

# flake8: noqa E501
COMMANDS = {
    "format": "normlib.bin.normdb.migrator:FormatDB",
    "migrate": "normlib.bin.normdb.migrator:MigrateDB",
    "hhbid get": "normlib.bin.normdb.hhbid:GetHHbID",
    "hhbid set": "normlib.bin.normdb.hhbid:SetHHbID",
    "dump": "normlib.bin.normdb.dump:DumpHHbEntity",
    "snapshot": "normlib.bin.normdb.snapshot:TakeSnapshot",
    "qa all": "normlib.bin.normdb.qa:QAAll",
    "qa missing_attributes": "normlib.bin.normdb.qa:QAMissingRequiredAttributes",
    "qa relation_cardinalities": "normlib.bin.normdb.qa:QARelationCardinalities",
    "qa illegal_attributes": "normlib.bin.normdb.qa:QAIllegalAttributes",
    "qa illegal_relations": "normlib.bin.normdb.qa:QAIllegalRelations",
    "qa external_ids": "normlib.bin.normdb.qa:QADuplicateExternalIDs",
    "qa ownership": "normlib.bin.normdb.qa:QAOwnership",
    "qa zombies": "normlib.bin.normdb.qa:QAZombies",
    "qa revlog_integrity": "normlib.bin.normdb.qa:QARevisionLogIntegrity",
    "qa analyse data": "normlib.bin.normdb.qa:QADataAnalyser",
    }

LOG = logging.getLogger(__name__)


class DictCommandManager(CommandManager):  # type: ignore
    """Manages commands and handles lookup based on argv data.

    :param commands: dict containing the mapping from command name to
                     implementing class.
    :param convert_underscores: Whether cliff should convert underscores to
                                spaces in commands.
    """

    def __init__(self, commands: Dict[str, str],
                 convert_underscores: bool = True):
        self.commands = dict()  # type: Dict[str, pkg_resources.EntryPoint]
        self._legacy = dict()  # type: Dict[str, str]
        self.namespace = None
        self.convert_underscores = convert_underscores
        self._load_commands(commands)

    def _load_commands(self, commands: Dict[str, str]) -> None:
        """Process provided commands

        :param commands: dict containing the mapping from command name to
                         implementing class.
        """
        for command, cls_path in commands.items():
            cmd_name = (command.replace("_", " ")
                        if self.convert_underscores else command)
            LOG.debug("Processing command %s", cmd_name)
            try:
                cls_module, cls_name = cls_path.split(":")
                self.commands[cmd_name] = EntryPointWrapper(
                    cmd_name, getattr(
                        importlib.import_module(cls_module), cls_name))
            except (ImportError, AttributeError) as e:
                LOG.exception(e, exc_info=False)
        LOG.debug("Processed commands: %s", self.commands)

    def load_commands(self, namespace: str) -> NoReturn:
        raise AttributeError

    def add_command(self, name: str, command_class: type) -> None:
        self.commands[name] = EntryPointWrapper(name, command_class)

    def add_legacy_command(self, old_name: str, new_name: str) -> NoReturn:
        raise AttributeError


class NormDBCLI:
    class __NormDBApp(App):  # type: ignore
        config = None  # type: configparser.ConfigParser

        def __init__(self) -> None:
            command_manager = DictCommandManager(COMMANDS)

            super().__init__(
                description=__description__, version=__version__,
                command_manager=command_manager)

        def build_option_parser(self, description: str, version: str) \
                -> argparse.ArgumentParser:
            parser = super().build_option_parser(
                description, version)  # type: argparse.ArgumentParser

            # TODO

            return parser

        def initialize_app(self, remainder: List[str]) -> None:
            # TODO
            pass

        def clean_up(self, cmd: Command, result: int,
                     err: Optional[Exception]) -> None:
            # TODO
            pass

        # Custom methods

        # TODO

    _instance = None  # type: __NormDBApp

    @classmethod
    def _init(cls) -> None:
        if not cls._instance:
            cls._instance = cls.__NormDBApp()

    @classmethod
    def instance(cls) -> "__NormDBApp":
        return cls._instance


def main(argv: List[str] = sys.argv[1:]) -> int:
    NormDBCLI._init()
    return int(NormDBCLI.instance().run(argv))
