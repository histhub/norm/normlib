import datetime

from typing import (Any, Iterator, Optional, Tuple)


class Revision:
    __slots__ = ("_id", "_message", "_created_at", "_commited_at")

    def __init__(self, message: str,
                 id: Optional[int] = None,
                 created_at: datetime.datetime = datetime.datetime.now(),
                 commited_at: Optional[datetime.datetime] = None) -> None:
        if id is not None and id <= 0:
            raise ValueError("id must be a positive integer greater than 0")
        self._id = id

        if not message:
            raise ValueError("message is required and must be non-empty")
        self._message = message

        if created_at is None:
            raise ValueError("created_at is a required parameter")
        self._created_at = created_at
        self._commited_at = commited_at

    @property
    def id(self) -> Optional[int]:
        return self._id

    @property
    def message(self) -> str:
        return self._message

    @property
    def created_at(self) -> datetime.datetime:
        return self._created_at

    @property
    def commited_at(self) -> Optional[datetime.datetime]:
        return self._commited_at

    @commited_at.setter
    def commited_at(self, new_value: datetime.datetime) -> None:
        self._commited_at = new_value

    def __iter__(self) -> Iterator[Tuple[str, Any]]:
        yield from ((a.lstrip("_"), getattr(self, a)) for a in self.__slots__)
