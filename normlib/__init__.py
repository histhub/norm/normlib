# Metadata

__description__ = "Library for accessing and managing the histHub norm DB"

__author__ = "SSRQ-SDS-FDS Law Sources Foundation of the Swiss Lawyers Society"
__copyright__ = ("Copyright 2019-2021 %s" % __author__)
__license__ = "GPLv3"
__maintainer__ = __author__
# Versions as per PEP 440 (https://www.python.org/dev/peps/pep-0440/)
__version_info__ = (1, 0, 1)
__version__ = "1.0.1"


# Types

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import (AnyStr, Optional, NoReturn, SupportsInt, Union)


class HistHubID(int):
    def __new__(cls, x: "Optional[Union[AnyStr, SupportsInt]]" = 0) \
            -> "HistHubID":
        res = int.__new__(cls)  # type: HistHubID
        try:
            res = int.__new__(cls, x)  # type: ignore
        except ValueError:
            raise ValueError(
                "invalid literal for HistHubID(): %r" % x) from None
        if res < 0:
            raise ValueError("HistHubID cannot be negative.")
        return res

    def __add__(self, other: int) -> "HistHubID":
        return self.__class__(super().__add__(other))

    def __sub__(self, other: int) -> "HistHubID":
        return self.__class__(super().__sub__(other))

    def __mul__(self, other: int) -> "HistHubID":
        return self.__class__(super().__mul__(other))

    def __truediv__(self, other: int) -> "NoReturn":
        raise TypeError("cannot divide HistHubIDs. Use X//Y instead.")

    def __floordiv__(self, other: int) -> "HistHubID":
        return self.__class__(super().__floordiv__(other))

    def __invert__(self) -> "HistHubID":
        return self.__class__(super().__xor__(2**self.bit_length()-1))

    def __neg__(self) -> "NoReturn":
        raise TypeError("HistHubIDs cannot be negated")
