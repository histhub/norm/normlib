CREATE FUNCTION pg_temp.illegal_relations(model_id integer)
	RETURNS TABLE(
		object_id integer, object_entity_name varchar,
		relation_id integer, relation_name varchar, target_id integer,
		target_entity_name varchar, "defined_from" text[],
		"defined_to" text[])
AS $$
WITH model_ancestors (entity_id, parent_id) AS (
	WITH RECURSIVE parents (entity_id, parent_id) AS (
		SELECT id AS "entity_id", id AS "parent_id" FROM "model_entity"
	  UNION ALL
		SELECT entity_id, parent_id FROM "model_inheritance" mi
	  UNION ALL
		SELECT mi.entity_id, p.parent_id
		FROM model_inheritance mi, parents p
		WHERE mi.parent_id = p.entity_id
	)
	SELECT * FROM parents
)
SELECT
	"or".object_id,
	(SELECT "name" FROM "model_entity" WHERE id = osrc.entity_id) AS "object_entity_name",
	"or".relation_id,
	mr."name" AS "relation_name",
	"or".target_id,
	(SELECT "name" FROM "model_entity" WHERE id = otgt.entity_id) AS "target_entity_name",
	(
		SELECT array_agg(me."name")
		FROM "model_ancestors" a
		LEFT JOIN "model_entity" me ON me.id = a.entity_id
		WHERE a.parent_id = mr.source_id
		GROUP BY a.parent_id
	) AS "defined_from",
	(
		SELECT array_agg(me."name")
		FROM "model_ancestors" a
		LEFT JOIN "model_entity" me ON me.id = a.entity_id
		WHERE a.parent_id = mr.target_id
		GROUP BY a.parent_id
	) AS "defined_to"
FROM
	"object_relation" "or"
LEFT JOIN "object" osrc ON osrc.id = "or".object_id
LEFT JOIN "object" otgt ON otgt.id = "or".target_id
LEFT JOIN "model_relation" mr ON mr.id = "or".relation_id
WHERE mr.source_id NOT IN (SELECT parent_id FROM "model_ancestors" WHERE entity_id = osrc.entity_id)
   OR mr.target_id NOT IN (SELECT parent_id FROM "model_ancestors" WHERE entity_id = otgt.entity_id)
$$ LANGUAGE SQL;
