CREATE FUNCTION pg_temp.check_ownership(model_id int)
	RETURNS TABLE(object_id integer, owner_id integer, owner_name varchar, is_provider boolean)
AS $$
SELECT
	o.id AS "object_id",
	"or".target_id AS "owner_id",
	(
		SELECT value
		FROM "object_attribute"
		WHERE object_id = "or".target_id
		  AND attribute_id = (
			  SELECT id
			  FROM "model_attribute"
			  WHERE entity_id = (SELECT entity_id FROM "object" WHERE id = "or".target_id)
				AND "name" = 'name'
		  )
	),
	CASE pflag.value WHEN 'true' THEN TRUE WHEN 'false' THEN FALSE ELSE NULL END AS "is_provider"
FROM "object" o
LEFT JOIN "object_relation" "or" ON "or".object_id = o.id AND "or".relation_id = (
	SELECT id
	FROM "model_relation"
	WHERE source_id IN (SELECT parent_id FROM "model_ancestors" WHERE entity_id = o.entity_id)
	  AND "name" = 'owners'
)
LEFT JOIN "object_attribute" pflag ON pflag.object_id = "or".target_id AND pflag.attribute_id = (
	SELECT id
	FROM "model_attribute"
	WHERE entity_id IN (SELECT parent_id FROM "model_ancestors" WHERE entity_id = (SELECT entity_id FROM "object" WHERE id = "or".target_id))
	  AND "name" = 'is_provider'
)
WHERE o.entity_id IN (
		SELECT entity_id
		FROM "model_ancestors"
		WHERE parent_id = (
			SELECT id
			FROM "model_entity"
			WHERE "model_id" = model_id AND "name" = 'Owned'
		)
	)
  AND ("or".target_id IS NULL OR pflag.value IS NULL OR pflag.value != 'true')
$$ LANGUAGE SQL;
