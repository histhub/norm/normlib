CREATE FUNCTION pg_temp.find_zombies(model_id integer)
	RETURNS TABLE(object_id integer, entity_name varchar)
AS $$
WITH freestanding_classes (entity_id) AS (
	SELECT entity_id
	FROM "model_ancestors"
	WHERE parent_id = (
		SELECT id
		FROM "model_entity"
		WHERE "name" = 'Identifiable' AND model_id = model_id
	)
)
SELECT
	o.id AS "object_id",
	(SELECT "name" FROM model_entity WHERE id = o.entity_id) AS "entity_name"
FROM
	"object" o
WHERE o.entity_id NOT IN (SELECT * FROM freestanding_classes)
  AND NOT EXISTS (
	SELECT 1
	FROM object_relation "or"
	WHERE "or".target_id = o.id
);
$$ LANGUAGE SQL;
