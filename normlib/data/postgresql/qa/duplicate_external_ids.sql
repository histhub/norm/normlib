CREATE FUNCTION pg_temp.maybeint(id text)
	RETURNS varchar
AS $$
BEGIN
	RETURN CAST(id AS integer);
EXCEPTION
	WHEN OTHERS THEN
		RETURN id::varchar;
END
$$ LANGUAGE plpgsql IMMUTABLE;

CREATE OR REPLACE FUNCTION pg_temp.duplicate_external_ids(model_id integer)
	RETURNS TABLE(institution varchar, institution_id varchar, object_ids integer[])
AS $$
SELECT
	(
		SELECT
			oa."value"
		FROM
			"object_attribute" oa
		WHERE oa.object_id = or_inst.target_id
		  AND oa.attribute_id = (
			  SELECT id
			  FROM "model_attribute"
			  WHERE entity_id = (SELECT entity_id FROM "object" WHERE id = oa.object_id)
				AND "name" = 'name'
		  )
	) AS "institution",
	oa_extid.value_ AS "institution_id",
	array_agg(or_extid.object_id) AS "object_ids"
FROM
	"object_relation" or_extid
JOIN "object_relation" or_inst
	ON or_inst.object_id = or_extid.target_id AND or_inst.relation_id = (
		SELECT id
		FROM "model_relation"
		WHERE source_id = (SELECT entity_id FROM "object" WHERE id = or_inst.object_id)
		  AND "name" = 'institution'
	)
JOIN LATERAL (
	SELECT oa_extid.*, pg_temp.maybeint(oa_extid.value) AS value_ FROM "object_attribute" oa_extid WHERE oa_extid.attribute_id = (
		SELECT id
		FROM "model_attribute"
		WHERE entity_id = (SELECT entity_id FROM "object" WHERE id = oa_extid.object_id)
		  AND "name" = 'external_id'
	)
) oa_extid ON oa_extid.object_id = or_extid.target_id
WHERE or_extid.relation_id IN (
	SELECT id FROM "model_relation" WHERE target_id = (
		SELECT id
		FROM "model_entity"
		WHERE "name" = 'ExternalIdentifier' AND model_id = model_id
	) AND "name" = 'external_ids'
)
GROUP BY (or_inst.target_id, oa_extid.value_)
HAVING count(*) > 1
$$ LANGUAGE SQL;
