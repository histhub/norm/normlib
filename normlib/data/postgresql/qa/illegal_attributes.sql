CREATE FUNCTION pg_temp.illegal_attributes(model_id integer)
	RETURNS TABLE(
		object_id integer, entity_name varchar, attribute_id integer,
		attribute_name varchar, defined_on text[])
AS $$
WITH model_ancestors (entity_id, parent_id) AS (
	WITH RECURSIVE parents (entity_id, parent_id) AS (
		SELECT id AS "entity_id", id AS "parent_id" FROM "model_entity"
	  UNION ALL
		SELECT entity_id, parent_id FROM "model_inheritance" mi
	  UNION ALL
		SELECT mi.entity_id, p.parent_id
		FROM model_inheritance mi, parents p
		WHERE mi.parent_id = p.entity_id
	)
	SELECT * FROM parents
)
SELECT
	oa.object_id,
	(SELECT "name" FROM "model_entity" WHERE id = o.entity_id) AS "entity_name",
	oa.attribute_id,
	ma."name" AS "attribute_name",
	(
		SELECT array_agg(me."name")
		FROM "model_ancestors" a
		LEFT JOIN "model_entity" me ON me.id = a.entity_id
		WHERE a.parent_id = ma.entity_id
		GROUP BY a.parent_id
	) AS "defined_on"
FROM
	"object_attribute" oa
LEFT JOIN "object" o ON o.id = oa.object_id
LEFT JOIN "model_attribute" ma ON ma.id = oa.attribute_id
WHERE ma.entity_id NOT IN (
	SELECT parent_id
	FROM "model_ancestors"
	WHERE entity_id = o.entity_id
)
$$ LANGUAGE SQL;
