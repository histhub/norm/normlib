CREATE FUNCTION pg_temp.incorrect_object_relation_cardinalities(
		model_id integer)
	RETURNS TABLE(
		object_id integer, entity_name varchar, relation_id integer,
		relation_name varchar, card_min smallint, card_max smallint,
		"count" bigint)
AS $$
WITH aggr_ancestors (entity_id, ancestor_ids) AS (
	SELECT entity_id, array_agg(parent_id)
	FROM "model_ancestors"
	GROUP BY entity_id
)
SELECT
	o.id AS "object_id",
	(SELECT "name" FROM "model_entity" WHERE id = o.entity_id) AS "entity_name",
	mr.id AS "relation_id",
	mr."name" AS "relation_name",
	mr.card_min, mr.card_max,
	COALESCE("or"."count", 0) AS "count"
FROM
	"object" o
JOIN "model_relation" mr ON mr.source_id IN (
	SELECT UNNEST(ancestor_ids)
	FROM aggr_ancestors
	WHERE entity_id = o.entity_id
)
LEFT JOIN (
	SELECT
		object_id, relation_id, count(*) AS "count"
	FROM "object_relation"
	GROUP BY (object_id, relation_id)
) "or" ON "or".object_id = o.id AND "or".relation_id = mr.id
WHERE
	CASE WHEN mr.card_max > 0 THEN
		COALESCE("or"."count", 0) NOT BETWEEN mr.card_min AND mr.card_max
	ELSE
		COALESCE("or"."count", 0) < mr.card_min -- no upper bound
	END;
$$ LANGUAGE SQL;
