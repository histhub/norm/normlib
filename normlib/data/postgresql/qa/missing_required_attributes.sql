CREATE FUNCTION pg_temp.missing_required_attributes(model_id integer)
	RETURNS TABLE(object_id integer, attribute_id integer, attribute_name varchar, is_required boolean, "count" bigint)
AS $$
WITH aggr_ancestors (entity_id, ancestor_ids) AS (
	SELECT entity_id, array_agg(parent_id)
	FROM "model_ancestors"
	GROUP BY entity_id
)
SELECT
	o.id AS "object_id",
	ma.id AS "attribute_id",
	ma."name" AS "attribute_name",
	ma.required AS "is_required",
	COALESCE(oa."count", 0) AS "count"
FROM
	"object" o
JOIN "model_attribute" ma ON ma.entity_id IN (
	SELECT unnest(ancestor_ids)
	FROM aggr_ancestors
	WHERE entity_id = o.entity_id
)
LEFT JOIN (
	SELECT
		object_id, attribute_id, count(*) AS "count"
	FROM "object_attribute"
	GROUP BY (object_id, attribute_id)
) oa ON oa.object_id = o.id AND oa.attribute_id = ma.id
WHERE ma.required AND NOT (oa."count" IS NOT NULL AND oa."count" > 0);
$$ LANGUAGE SQL;
