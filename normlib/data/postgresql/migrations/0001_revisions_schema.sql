-- -*- mode: sql; sql-product: postgres; indent-tabs-mode: nil -*-
--
-- PostgreSQL database schema for normlib (sidecar DB).
-- 0001: Create revision tables.
--

--
-- Name: revision; Type: TABLE
--

CREATE TABLE {SIDECAR_SCHEMA_NAME}.revision (
    id integer NOT NULL, -- PRIMARY KEY
    created_at timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    commited_at timestamp with time zone NULL DEFAULT NULL,
    message text NOT NULL,
    CONSTRAINT revision_pkey PRIMARY KEY (id),
    CONSTRAINT revision_timestamps_check CHECK (commited_at > created_at)
);


-- Assert that when a new revision is created, there is only one uncommited
-- revision at a time.
CREATE FUNCTION {SIDECAR_SCHEMA_NAME}.assert_only_one_uncommited_revision()
RETURNS trigger AS $$
BEGIN
    IF EXISTS (SELECT 1 FROM {SIDECAR_SCHEMA_NAME}.revision
               WHERE commited_at IS NULL) THEN
        IF NEW.commited_at IS NULL THEN
           RAISE EXCEPTION 'Only one uncommited revision at a time is allowed.';
        ELSE
           RAISE EXCEPTION 'Cannot create an immediately commited revision.';
        END IF;
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER new_revision
    BEFORE INSERT
    ON {SIDECAR_SCHEMA_NAME}.revision
    FOR EACH ROW
    EXECUTE PROCEDURE {SIDECAR_SCHEMA_NAME}.assert_only_one_uncommited_revision();


-- Upon creation of a new revision assert that the revision IDs are a strictly
-- monotonic sequence.
CREATE FUNCTION {SIDECAR_SCHEMA_NAME}.assert_revision_id_monotonicity()
RETURNS trigger AS $$
DECLARE
  max_id integer;
BEGIN
    SELECT COALESCE(max(id), 0) INTO max_id FROM {SIDECAR_SCHEMA_NAME}.revision;

    IF NEW.id <= max_id THEN
       RAISE EXCEPTION 'Revision ID must be a strictly monotonic sequence! '
                       'At least % required, got %.', (max_id + 1), NEW.id;
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER new_revision_id
    BEFORE INSERT
    ON {SIDECAR_SCHEMA_NAME}.revision
    FOR EACH ROW
    EXECUTE PROCEDURE {SIDECAR_SCHEMA_NAME}.assert_revision_id_monotonicity();


-- Assert that revision IDs are immutable.
CREATE FUNCTION {SIDECAR_SCHEMA_NAME}.assert_revision_id_immutability()
RETURNS trigger AS $$
BEGIN
    IF OLD.id <> NEW.id THEN
        RAISE EXCEPTION 'Revision ID is immutable!';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER revision_id_change
    BEFORE UPDATE OF id
    ON {SIDECAR_SCHEMA_NAME}.revision
    FOR EACH ROW
    EXECUTE PROCEDURE {SIDECAR_SCHEMA_NAME}.assert_revision_id_immutability();


-- Assert that commited revisions cannot be changed.
CREATE FUNCTION {SIDECAR_SCHEMA_NAME}.assert_commited_revision_is_immutable()
RETURNS trigger AS $$
BEGIN
    IF OLD.commited_at IS NOT NULL AND OLD.* IS DISTINCT FROM NEW.* THEN
       RAISE EXCEPTION 'Revision % is commited and thus cannot be mutated!',
                       OLD.id;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER revision_commit_state_change
    BEFORE UPDATE OF commited_at
    ON {SIDECAR_SCHEMA_NAME}.revision
    FOR EACH ROW
    EXECUTE PROCEDURE {SIDECAR_SCHEMA_NAME}.assert_commited_revision_is_immutable();


--- Name: revision_id_seq; Type: SEQUENCE
---

CREATE SEQUENCE {SIDECAR_SCHEMA_NAME}.revision_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    MINVALUE 1
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE {SIDECAR_SCHEMA_NAME}.revision_id_seq
    OWNED BY {SIDECAR_SCHEMA_NAME}.revision.id;
ALTER TABLE ONLY {SIDECAR_SCHEMA_NAME}.revision
    ALTER COLUMN id
    SET DEFAULT nextval('{SIDECAR_SCHEMA_NAME}.revision_id_seq'::regclass);


--
-- Name: job_state; Type: TYPE
--

CREATE TYPE {SIDECAR_SCHEMA_NAME}.action AS ENUM (
    'created',
    'modified',
    'deleted'
);


--
-- Name: log; Type: TABLE
--

CREATE TABLE {SIDECAR_SCHEMA_NAME}.log (
    revision_id integer NOT NULL,
    histhub_id integer NOT NULL,
    action {SIDECAR_SCHEMA_NAME}.action NOT NULL,
    message text NULL,
    CONSTRAINT log_revision_id_fkey FOREIGN KEY (revision_id)
        REFERENCES {SIDECAR_SCHEMA_NAME}.revision(id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT only_one_log_per_entity_and_revision
        UNIQUE (revision_id, histhub_id)
);

CREATE INDEX log_revision_id_idx ON {SIDECAR_SCHEMA_NAME}.log USING btree (revision_id);


-- Assert that log entries are only added to not-yet-commited revisions.
CREATE FUNCTION {SIDECAR_SCHEMA_NAME}.assert_commited_revision_log_is_immutable()
RETURNS trigger AS $$
DECLARE
    revision_id integer;
BEGIN
    IF (TG_OP = 'DELETE') THEN
        revision_id = OLD.revision_id;
    ELSIF (TG_OP = 'INSERT') THEN
        revision_id = NEW.revision_id;
    END IF;

    IF (
        SELECT r.commited_at
        FROM {SIDECAR_SCHEMA_NAME}.revision r
        WHERE r.id = revision_id
    ) IS NOT NULL THEN
        RAISE EXCEPTION 'Cannot modify the log of commited revision %!',
                        revision_id;
    END IF;

    IF (TG_OP != 'DELETE') THEN RETURN NEW; ELSE RETURN OLD; END IF;
END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER check_immutability_of_commited_revision
    BEFORE INSERT OR UPDATE OR DELETE
    ON {SIDECAR_SCHEMA_NAME}.log
    FOR EACH ROW
    EXECUTE PROCEDURE {SIDECAR_SCHEMA_NAME}.assert_commited_revision_log_is_immutable();


-- FIXME: Ensure that even name of commited revision cannot be changed
