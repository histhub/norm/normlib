-- -*- mode: sql; sql-product: postgres; indent-tabs-mode: nil -*-
--
-- PostgreSQL database schema for normlib (sidecar DB).
-- 0002: Add histHub ID generation sequence
--

--
-- Name: histhub_id_seq; Type: SEQUENCE
--

CREATE SEQUENCE {SIDECAR_SCHEMA_NAME}.histhub_id_seq
    AS integer
    INCREMENT BY 1
    START WITH 10000000
    NO CYCLE
    OWNED BY NONE;

-- NOTE: We update the sequence's value to guarantee that it is greater than the
--       largest histHub ID in the log, in case the histHub sequence is
--       created after entities have already been imported.
SELECT
    setval('{SIDECAR_SCHEMA_NAME}.histhub_id_seq', m.max_hhb_id, false)
FROM (
    SELECT
        COALESCE(
            max(histhub_id) + 1,
            nextval('{SIDECAR_SCHEMA_NAME}.histhub_id_seq')
        ) AS max_hhb_id
    FROM
        {SIDECAR_SCHEMA_NAME}.log
) m;


CREATE FUNCTION {SIDECAR_SCHEMA_NAME}.nextval_multi(seq regclass, count integer)
    RETURNS SETOF bigint
    VOLATILE
    PARALLEL UNSAFE
    LANGUAGE SQL
AS $$
SELECT nextval(seq) FROM generate_series(1, count);
$$;

-- SELECT nextval('sidecar.histhub_id_seq'), sidecar.nextval_multi('sidecar.histhub_id_seq', 5);
-- returns:
--  |nextval             |nextval_multi       |
--  |--------------------|--------------------|
--  |10000000            |10000001            |
--  |10000002            |10000003            |
--  |10000004            |10000005            |
--  |10000006            |10000007            |
--  |10000008            |10000009            |



CREATE FUNCTION {SIDECAR_SCHEMA_NAME}.request_histhub_ids(count integer)
    RETURNS SETOF bigint
    VOLATILE
    RETURNS NULL ON NULL INPUT
    PARALLEL UNSAFE
    LANGUAGE plpgsql
AS $$
BEGIN
    IF count < 1 THEN
       RAISE EXCEPTION 'You must request at least 1 histHub ID!';
    END IF;

    RETURN QUERY (SELECT {SIDECAR_SCHEMA_NAME}.nextval_multi(
            '{SIDECAR_SCHEMA_NAME}.histhub_id_seq', count)
        );
END
$$;
