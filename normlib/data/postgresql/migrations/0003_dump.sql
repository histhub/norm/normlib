-- -*- mode: sql; sql-product: postgres; indent-tabs-mode: nil -*-
--
-- PostgreSQL database schema for normlib (sidecar DB).
-- 0002: Create entity dump table.

--
-- Name: dump; Type: TABLE
--

CREATE TABLE {SIDECAR_SCHEMA_NAME}.dump (
    id integer NOT NULL, -- PRIMARY KEY
    histhub_id integer NOT NULL,
    revision_id integer NOT NULL,
    entity_dump xml NOT NULL CHECK (xml_is_well_formed_document(entity_dump::text)),
    CONSTRAINT dump_pkey PRIMARY KEY (id),
    CONSTRAINT dump_revision_id_fkey FOREIGN KEY (revision_id)
        REFERENCES {SIDECAR_SCHEMA_NAME}.revision(id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT only_one_dump_per_entity_and_revision
        UNIQUE (histhub_id, revision_id)
);


--
-- Name: dump_id_seq; Type: SEQUENCE
--

CREATE SEQUENCE {SIDECAR_SCHEMA_NAME}.dump_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    MINVALUE 1
    NO MAXVALUE
    CACHE 1
    NO CYCLE
    OWNED BY {SIDECAR_SCHEMA_NAME}.dump.id;

ALTER TABLE ONLY {SIDECAR_SCHEMA_NAME}.dump
    ALTER COLUMN id
    SET DEFAULT nextval('{SIDECAR_SCHEMA_NAME}.dump_id_seq'::regclass);


--
-- Name: search_graph; Type: FUNCTION
--

CREATE FUNCTION {SIDECAR_SCHEMA_NAME}.search_graph (
    an_object_id integer,
    a_max_depth integer DEFAULT 20,
    a_terminals text[] DEFAULT ARRAY['Identifiable']
)
RETURNS TABLE (
    "object_id" integer, "relation_id" integer, "target_id" integer,
    "depth" integer, "path" integer[], "cycle" boolean, "terminal" boolean
)
RETURNS NULL ON NULL INPUT
AS $$
WITH RECURSIVE stop_entities ("entity_id") AS (
  SELECT DISTINCT entity_id
  FROM {DATA_SCHEMA_NAME}."model_ancestors"
  WHERE parent_id IN (
    SELECT "id"
    FROM {DATA_SCHEMA_NAME}."model_entity"
    WHERE model_id = (
      SELECT model_id
      FROM {DATA_SCHEMA_NAME}."model_entity"
      WHERE "id" = (
        SELECT entity_id
        FROM {DATA_SCHEMA_NAME}."object"
        WHERE "id" = an_object_id
      )
    )
    AND "name" = ANY(a_terminals)
  )
), relations (
    "object_id", "relation_id", "target_id", "depth", "path", "cycle",
    "terminal"
  ) AS (
    SELECT
      "or".object_id, "or".relation_id, "or".target_id, 1,
      ARRAY["or".object_id, "or".target_id],
      ("or".target_id = "or".object_id),
      EXISTS (
        SELECT 1
        FROM stop_entities
        WHERE entity_id = (
          SELECT entity_id
          FROM {DATA_SCHEMA_NAME}."object"
          WHERE "id" = "or".target_id
        )
      )
    FROM {DATA_SCHEMA_NAME}."object_relation" "or"
    WHERE "or".object_id = an_object_id AND a_max_depth > 1
  UNION ALL
    SELECT
      nxt.object_id, nxt.relation_id, nxt.target_id, prv."depth" + 1,
      prv."path" || nxt.target_id,
      nxt.target_id = ANY(prv."path"),
      EXISTS (
        SELECT 1
        FROM stop_entities
        WHERE entity_id = (
          SELECT entity_id
          FROM {DATA_SCHEMA_NAME}."object"
          WHERE "id" = nxt.target_id
        )
      )
    FROM relations prv
    JOIN {DATA_SCHEMA_NAME}."object_relation" nxt ON nxt.object_id = prv.target_id
    LEFT JOIN {DATA_SCHEMA_NAME}."object" o ON o.id = nxt.object_id
    WHERE prv."depth" < a_max_depth AND NOT prv."cycle" AND NOT prv."terminal"
)
SELECT *
FROM relations
WHERE "depth" <= a_max_depth -- "HACK" for max_depth = 0
ORDER BY "depth", object_id
$$ LANGUAGE sql;

--
-- Name: map_object_relations; Type: FUNCTION
--

-- NOTE: Essentialy the same as search_graph(), but the path is dropped,
--       thus relations are distinct.
CREATE FUNCTION {SIDECAR_SCHEMA_NAME}.map_object_relations (
    an_object_id integer,
    a_max_depth integer DEFAULT 20,
    a_terminals text[] DEFAULT ARRAY['Identifiable']
)
RETURNS TABLE (
    "object_id" integer, "relation_id" integer, "target_id" integer,
    "depth" integer, "cycle" boolean, "terminal" boolean
)
RETURNS NULL ON NULL INPUT
AS $$
SELECT DISTINCT
  "object_id",
  "relation_id",
  "target_id",
  "depth",
  "cycle",
  "terminal"
FROM
  {SIDECAR_SCHEMA_NAME}.search_graph(an_object_id, a_max_depth, a_terminals)
ORDER BY
  "depth", "object_id"
$$ LANGUAGE sql;
