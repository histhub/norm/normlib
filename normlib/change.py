import enum

from typing import (Any, Iterator, Tuple)

from normlib.revision import Revision


@enum.unique
class Action(enum.Enum):
    CREATED = "created"
    MODIFIED = "modified"
    DELETED = "deleted"

    def __str__(self) -> str:
        return str(self.value)


class ChangedEntity:
    __slots__ = ("_histhub_id", "_revision", "_action", "_message")

    def __init__(self, histhub_id: int, revision: Revision, action: Action,
                 message: str) -> None:
        if not histhub_id or histhub_id < 0:
            raise ValueError("histhub_id must be a positive integer")
        self._histhub_id = histhub_id

        if revision is None:
            raise ValueError("revision is a required parameter")
        self._revision = revision

        if action is None:
            raise ValueError("action is a required parameter")
        if not isinstance(action, Action):
            raise TypeError("action must be an Action")
        self._action = action

        if not message:
            raise ValueError("message is required and must be non-empty")
        self._message = message

    @property
    def histhub_id(self) -> int:
        return self._histhub_id

    @property
    def action(self) -> Action:
        return self._action

    @property
    def message(self) -> str:
        return self._message

    @property
    def revision(self) -> Revision:
        return self._revision

    def __iter__(self) -> Iterator[Tuple[str, Any]]:
        yield from ((a.lstrip("_"), getattr(self, a)) for a in self.__slots__)
