import abc
import difflib
import xml.dom.minidom

from typing import (Any, Iterable, Iterator, Optional, Text, Type)

from normlib import HistHubID
from normlib.change import ChangedEntity
from normlib.connection import NormDBConnection
from normlib.db.sidecar_db import SidecarDBBackend
from normlib.dump import DeepNodeSerialiser
from normlib.revision import Revision


class EntityManager(metaclass=abc.ABCMeta):
    def __new__(cls, norm_db: NormDBConnection,
                *args: Any, **kwargs: Any) -> "EntityManager":
        """Creates a new EntityManager based on the provided DB connection.

        :param norm_db: A NormDBConnection
        :type norm_db: normlib.connection.NormDBConnection
        :raises NotImplementedError: when the DB type is not (yet) supported
        :returns: EntityManager -- a new EntityManager instance
        """

        if cls is not EntityManager:
            # __new__ of subclasses will fall through to this __new__ and
            # create infinite recursion
            return object.__new__(cls)  # type: ignore

        if not isinstance(norm_db, NormDBConnection):
            raise TypeError("norm_db must be a NormDBConnection")

        real_cls = None  # type: Optional[Type[EntityManager]]

        # FIXME: Should probably not use sidecar DB to determine NormDB backend
        backend = norm_db.sidecar_db.backend

        if backend is SidecarDBBackend.POSTGRESQL:
            from normlib.db.pgsql.entity_manager import PgSQLEntityManager
            real_cls = PgSQLEntityManager
        # elif backend is StateDBBackend.SQLITE:
        #     from normlib.db.sqlite.entity_manager import SQLiteLogManager
        #     real_cls = SQLiteLogManager

        if real_cls is not None:
            return real_cls.__new__(real_cls, norm_db, *args, **kwargs)

        raise NotImplementedError("This DB dialect is not supported")

    def __init__(self, norm_db: NormDBConnection) -> None:
        self.norm_db = norm_db

    def all_entities(self) -> Iterable[HistHubID]:
        """Return the histHub IDs of all entities in the norm_db.

        :returns: Iterable[HistHubID] -- the histHub IDs of the entities.
        """
        g = self.norm_db.graph.traversal()
        return map(HistHubID, g.v().has_label("Identifiable").values("hhb_id"))

    @abc.abstractmethod
    def last_change(self, hhb_id: HistHubID) -> ChangedEntity:
        """Return the last change that touched the entity (node_id).

        :param hhb_id: the histhub ID
        :type hhb_id: normlib.HistHubID
        :returns: ChangedEntity -- the last change
        """

    @staticmethod
    def _xmlpretty(xml_str: str) -> str:
        return str(xml.dom.minidom.parseString(xml_str).toprettyxml(
            encoding="utf-8").decode())

    def diff(self, hhb_id: HistHubID,
             old_rev: Optional[Revision] = None,
             new_rev: Optional[Revision] = None) -> "Iterator[Text]":
        """Return a diff of entity with hhb_id between old_rev and new_rev.

        :param old_rev: the old revision (defaults to newest revision)
        :type old_rev: Optional[normlib.revision.Revision]
        :param new_rev: the new revision (defaults to live state)
        :type new_rev: Optional[normlib.revision.Revision]
        :returns: Generator[str] -- unified diff lines
        """
        dump_manager = self.norm_db.dump_manager()

        g = self.norm_db.graph.traversal()
        node_id = next(g.v().has("hhb_id", hhb_id).id())

        # fetch old_rev state
        if old_rev is None:
            entity_manager = self.norm_db.entity_manager()
            old_rev = entity_manager.last_change(hhb_id).revision
            assert old_rev.id
        last_xml = self._xmlpretty(dump_manager.get_snapshot(hhb_id, old_rev))

        # fetch new_rev state
        if new_rev is not None:
            curr_xml = self._xmlpretty(dump_manager.get_snapshot(
                hhb_id, new_rev))
        else:
            curr_dn = dump_manager.dump_object(node_id)
            if not curr_dn:
                raise RuntimeError("Failed to dump node %u" % node_id)
            curr_xml = DeepNodeSerialiser.as_xml(
                curr_dn, enable_hidden=True, pretty=True)

        if last_xml == curr_xml:
            return iter(())

        return difflib.unified_diff(
            last_xml.splitlines(keepends=True),
            curr_xml.splitlines(keepends=True),
            fromfile=("urn:histhub:%u@r%u" % (
                hhb_id, old_rev.id or 0)),
            tofile=("urn:histhub:%u@%s" % (
                hhb_id,
                ("r%u" % new_rev.id if new_rev and new_rev.id else "live"))))
