import abc
import enum

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from eavlib.db.url import URL
    from typing import (Any, Callable, Dict, Iterator, List)


class MigrationError(RuntimeError):
    pass


class SidecarDBMigrator(metaclass=abc.ABCMeta):
    Hooks = enum.Enum("Hooks", {
        "before_migration": "before_migration",
        "after_migration": "after_migration",
        "failed_migration": "failed_migration",
        "log_message": "log_message",
        })

    _latest_version = 3

    def __new__(cls, url: "URL", data_schema: str,
                debug: bool = False) -> "SidecarDBMigrator":
        if cls is not SidecarDBMigrator:
            # __new__ of subclasses will fall through to this __new__ and
            # create infinite recursion
            return object.__new__(cls)  # type: ignore

        dialect = url.get_backend_name()

        if dialect == "postgresql":
            from normlib.db.pgsql.migrator import PgSQLSidecarDBMigrator
            return PgSQLSidecarDBMigrator(url, data_schema, debug)
        else:
            raise NotImplementedError(
                "Connection dialect %s is not supported" % (dialect))

    def __init__(self, url: "URL", data_schema: str, debug: bool) -> None:
        self.url = url
        self.hooks = {hook: [] for hook in self.Hooks} \
            # type: Dict[SidecarDBMigrator.Hooks, List[Callable[..., None]]]

    def _call_hooks(self, name: "SidecarDBMigrator.Hooks",
                    *args: "Any", **kwargs: "Any") -> None:
        for hook in filter(callable, self.hooks[self.Hooks(name)]):
            hook(*args, **kwargs)

    def register_hooks(self, **kwargs: "Callable[..., None]") -> None:
        """Register hook functions to be called by the migrator.

        :param kwargs: name=hook pair of the hook to register a callback for.
        :type kwargs: callable
        """
        for name, hook in kwargs.items():
            self.hooks[self.Hooks(name)].append(hook)

    @abc.abstractmethod
    def create(self) -> None:
        """Creates and prepares the schema specified in the URL."""

    @abc.abstractmethod
    def create_schema(self) -> None:
        """Creates the schema specified in the connection URL."""

    @abc.abstractmethod
    def schema_exists(self) -> bool:
        """Returns if the schema to be initialised exists.

        :returns: bool -- True if the schema exists in the RDBMS.
        """

    @abc.abstractmethod
    def is_created(self) -> bool:
        """Returns if the schema has been initialised.

        :returns: bool -- True if the schema has been initialised.
        """

    @abc.abstractmethod
    def destroy(self) -> None:
        """Destroys a schema by deleting it.

        NOTE: This method does not raise an exception if the schema does not
        exist.
        """

    @abc.abstractmethod
    def current_version(self) -> int:
        """Returns the current schema version.

        :returns: int -- the current schema version
        """

    def migration_needed(self, version: int = _latest_version) -> bool:
        """Returns whether a migration is needed to to get the schema to
        `version`.

        :param version: The target version. If omitted, the latest version.
        :type version: int
        :returns: bool -- True if a migration is needed.
        """
        return self.current_version() < version

    def migrate(self, version: int = _latest_version) -> None:
        """Migrate the schema to `version`.

        :param version: The target version. If omitted, the latest version.
        :type version: int
        """
        current = self.current_version()

        if current >= version:
            print("Schema is already at version %u. Nothing to do." % current)
            return

        if version > self._latest_version:
            raise ValueError(
                "There is no migration to version %u available!" % version)

        print("Schema is currently at version: %d" % current)

        for revision_version in range(current, version):
            # Migrate to next version
            self.run_migration(revision_version + 1)

    @abc.abstractmethod
    def run_migration(self, version: int) -> None:
        """Execute the migration scripts for a specific schema version.

        :param version: the schema version to execute migrations scripts for.
        :type version: int
        :raises MigrationError: on errors.
        """

    def _get_migration_resource_names(
            self, resdir: str, version: int) -> "Iterator[str]":
        import fnmatch
        import os
        import pkg_resources

        yield from map(
            lambda basename: os.path.join(resdir, basename),
            filter(
                lambda fn: fnmatch.fnmatch(fn, "%04u_*.sql" % version),
                pkg_resources.resource_listdir("normlib", resdir)))
