import abc

from eavlib.gremlin.traversal import P
from typing import TYPE_CHECKING

from normlib.connection import NormDBConnection
from normlib.db.sidecar_db import SidecarDBBackend
from normlib.dump import DeepNode

if TYPE_CHECKING:
    from typing import (Optional, Sequence, Type)
    from normlib import HistHubID
    from normlib.revision import Revision


class DumpManager(metaclass=abc.ABCMeta):
    def __new__(cls, norm_db: NormDBConnection) -> "DumpManager":
        """Creates a new HistHubIDManager based on the provided DB connection.

        :param norm_db: A NormDBConnection
        :type norm_db: normlib.connection.NormDBConnection
        :raises NotImplementedError: when the DB type is not (yet) supported
        :returns: HistHubIDManager -- a new HistHubIDManager instance
        """
        if cls is not DumpManager:
            # __new__ of subclasses will fall through to this __new__ and
            # create infinite recursion
            return object.__new__(cls)  # type: ignore

        if not isinstance(norm_db, NormDBConnection):
            raise TypeError("norm_db must be a NormDBConnection")

        real_cls = None  # type: Optional[Type[DumpManager]]
        # FIXME: Should probably not use sidecar DB to determine NormDB backend
        backend = norm_db.sidecar_db.backend

        if backend is SidecarDBBackend.POSTGRESQL:
            from normlib.db.pgsql.dump_manager import PgSQLDumpManager
            real_cls = PgSQLDumpManager
        # elif backend is SidecarDBBackend.SQLITE:
        #     from normlib.db.sqlite.dump_manager import SQLiteDumpManager
        #     real_cls = SQLiteDumpManager

        if real_cls is not None:
            return real_cls.__new__(real_cls, norm_db)

        raise NotImplementedError("This DB dialect is not supported")

    def __init__(self, norm_db: NormDBConnection) -> None:
        self.norm_db = norm_db

    @abc.abstractmethod
    def dump_object(self, node_id: int, max_depth: int = 20) \
            -> "Optional[DeepNode]":
        """Generate a dump of a histHub object and its relations.
        It traverses until a cycle or an Identifiable object is visited.

        This method differs from dump_entity in that it takes a node ID in
        place of a histHub ID.

        :param node_id: The ID of the node to dump.
        :type node_id: int
        :param max_depth: The maximum depth to traverse (> 0).
        :type max_depth: int
        """

    def dump_entity(self, hhb_id: "HistHubID", max_depth: int = 20) \
            -> "Optional[DeepNode]":
        """Generate a dump of a histHub entity and its relations.
        It traverses until a cycle or an Identifiable object is visited.

        This method differs from dump_object in that it takes a histHub ID in
        place of a node ID.

        :param hhb_id: The histHub ID of the entity to dump.
        :type hhb_id: normlib.HistHubID
        :param max_depth: The maximum depth to traverse (> 0).
        :type max_depth: int
        """
        try:
            node_id = next(
                self.norm_db.graph.traversal().v().has("hhb_id", hhb_id).id())
        except StopIteration:
            raise ValueError("invalid hhb_id: %u" % (hhb_id))
        return self.dump_object(node_id, max_depth)

    @abc.abstractmethod
    def store_dump(self, dump: DeepNode, revision: "Revision") -> None:
        """Store a dump of a histHub entity in the sidecar DB.

        :param dump: the DeepNode to store.
        :type dump: DeepNode
        :param revision: the revision this dump has created from.
        :type revision: Revision
        """

    def snapshot_entities(self, hhb_ids: "Sequence[HistHubID]",
                          max_depth: int = 20) -> None:
        """Snapshot entities, i.e. dump them and store the dump in the dump
        table of the sidecar DB.

        :param hhb_ids: the hhb IDs of the entities to snapshot.
        :type hhb_ids: Sequence[normlib.HistHubID]
        :param max_depth: The maximum depth to traverse (> 0).
        :type max_depth: int
        """
        revision_manager = self.norm_db.revision_manager()
        revision = revision_manager.latest_revision()
        if revision is None:
            raise RuntimeError("No revisions in database.")
        for node_id in self.norm_db.graph.traversal().v().has(
                "hhb_id", P.within(*hhb_ids)).id():
            dn = self.dump_object(node_id, max_depth)
            if dn is None:
                raise RuntimeError("Could not dump object %u." % node_id)
            self.store_dump(dn, revision)

    def snapshot_entity(self, hhb_id: "HistHubID",
                        max_depth: int = 20) -> None:
        """Snapshot a single entity, i.e. dump it and store the dump in the dump
        table of the sidecar DB.

        :param hhb_id: the histHub ID of the entity to snapshot.
        :type hhb_id: normlib.HistHubID
        :param max_depth: The maximum depth to traverse (> 0).
        :type max_depth: int
        """
        self.snapshot_entities([hhb_id], max_depth)

    def get_snapshot(self, hhb_id: "HistHubID", rev: "Revision") -> str:
        """Get the snapshot of an entity at a given revision.

        :param hhb_id: the histHub ID of the entity to get.
        :type hhb_id: normlib.HistHubID
        :param revision: the Revision
        :type revision: normlib.revision.Revision
        :returns: str -- the XML string.
        :raises: RuntimeError -- if no snapshot exists
        """
