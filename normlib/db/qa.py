import abc
import enum
import logging
import multiprocessing.pool

from normlib.connection import NormDBConnection
from normlib.db.sidecar_db import SidecarDBBackend

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import (Any, Dict, Generator, Iterable, List, Tuple)

    from normlib import HistHubID

    _Summary = Dict[str, int]  # test: num_errors
    _Tabular = Tuple[Tuple[str, ...], Tuple[Any, ...]]

LOG = logging.getLogger(__name__)


class NormDBQA(metaclass=abc.ABCMeta):
    Checkers = enum.Enum("QACheckers", {
        "missing_attributes": "missing_required_attributes",
        "relation_cardinalities": "incorrect_object_relation_cardinalities",
        "illegal_attributes": "illegal_attributes",
        "illegal_relations": "illegal_relations",
        "external_ids": "duplicate_external_ids",
        "ownership": "check_ownership",
        "zombies": "find_zombies",
        "revlog_integrity": "check_revision_log_integrity",
        })

    def __new__(cls, norm_db: NormDBConnection) -> "NormDBQA":
        """Creates a new NormDBQA instance.

        :param norm_db: A NormDBConnection
        :type norm_db: normdb.connection.NormDBConnection
        :returns: NormDBQA -- a new NormDBQA instance.
        """
        if cls is not NormDBQA:
            # __new__ of subclasses will fall through to this __new__ and
            # create infinite recursion
            return object.__new__(cls)  # type: ignore

        # FIXME: Should probably not use sidecar DB to determine NormDB backend
        backend = norm_db.sidecar_db.backend

        if backend is SidecarDBBackend.POSTGRESQL:
            from normlib.db.pgsql.qa import PgSQLNormDBQA
            return PgSQLNormDBQA(norm_db)

        raise NotImplementedError("This DB dialect is not supported")

    def __init__(self, norm_db: NormDBConnection) -> None:
        self.norm_db = norm_db

    def check_all(self, *, mp: int = 4) -> "Dict[str, List[Dict[str, Any]]]":
        """Run all DB checker functions from NormDBQA.Checkers.

        :param mp: the number of checkers to run in parallel
        :type mp: int
        :returns: Dict[str, List[Dict[str, Any]]]" -- A Dict containing the
        errors grouped by checker.
        """
        def _run_checker(checker: NormDBQA.Checkers) -> "List[Dict[str, Any]]":
            qa = NormDBQA(self.norm_db.copy())  # type: ignore
            cols, rows = qa.check(checker)

            return [
                {cols[i]: x for i, x in enumerate(row)}
                for row in rows]

        if mp < 1:
            raise ValueError("mp must be at least 1")

        with multiprocessing.pool.ThreadPool(processes=mp) as pool:
            return {
                checker.name: res
                for checker, res
                in zip(
                    self.Checkers,
                    pool.imap(_run_checker, self.Checkers))
                }

    def check(self, checker: Checkers) -> "_Tabular":
        """Run DB checker function.

        :param checker: One of the NormDBQA checkers.
        :type checker: normlib.db.qa.NormDBQA.Checkers
        :returns: normlib.db.qa._Tabular -- a "table" with the errors
        """
        funcname = checker.value
        if callable(getattr(self, funcname, None)):
            # Use "special" implementation
            return getattr(self, funcname)()  # type: ignore
        else:
            # Fall back to "plain" SQL functions
            return self._exec_sql_check(funcname)

    @abc.abstractmethod
    def _exec_sql_check(self, funcname: str) -> "_Tabular":
        """Internal method"""

    def check_revision_log_integrity(self) -> "_Tabular":
        entity_manager = self.norm_db.entity_manager()

        def _check_entities(hhb_ids: "Iterable[HistHubID]") \
                -> "Generator[Tuple[HistHubID], None, None]":
            for hhb_id in hhb_ids:
                diff = entity_manager.diff(hhb_id)
                if next(diff, None):
                    yield (hhb_id,)

        rows = tuple(_check_entities(entity_manager.all_entities()))
        return (("histhub_id",), rows)


class NormDBAnalyser:
    def __init__(self, norm_db: NormDBConnection) -> None:
        self.norm_db = norm_db

    def analyse(self, *, mp: int = 4) -> "_Summary":
        """Run all DB analyser functions.

        :param mp: the number of checkers to run in parallel
        :type mp: int
        :returns: _Summary -- A Dict mapping the analyser to the number of
        errors it reported.
        """

        def _run_analyser(funcname: str) -> int:
            analyser = self.__class__(self.norm_db.copy())
            func = getattr(analyser, funcname)  # Callable[[], int]

            # LOG.info("Analyser: " + func.__doc__)

            num_errors = int(func())
            # if not num_errors:
            #     LOG.info("  no errors")
            return num_errors

        if mp < 1:
            raise ValueError("mp must be at least 1")

        analysers = (
            "scan_place_within_itself",
            "scan_duplicate_appellations",
            )

        with multiprocessing.pool.ThreadPool(processes=mp) as pool:
            return {
                funcname: num_errors
                for funcname, num_errors
                in zip(
                    analysers,
                    pool.imap(_run_analyser, analysers))
                }

    def scan_duplicate_appellations(self) -> int:
        """Appellations with same lang + text."""
        import itertools
        from eavlib.gremlin.traversal import P

        g = self.norm_db.graph.traversal()

        def kf(m: "Dict[str, str]") -> "Tuple[str, ...]":
            return (m["lang"], m["text"])

        for key, hhb_ids in filter(
                lambda item: len(item[1]) > 1,
                ((k, tuple(int(d["hhb_id"]) for d in v))
                 for k, v
                 in itertools.groupby(
                     sorted(
                         g.v().has_label("Appellation").value_map(), key=kf),
                     key=kf))):
            classes = set(g.v().has("hhb_id", P.within(*hhb_ids)).label())

            if len(classes) > 1:
                continue  # no duplicate

            LOG.warning(
                "Possible %s %r duplicate: %r",
                next(iter(classes), "Appellation"),
                key, hhb_ids)

        return 0  # all finds are considered "warnings"

    def scan_place_within_itself(self) -> int:
        """Places having a spatial relation with themselves."""
        g = self.norm_db.graph.traversal()

        num_errors = 0

        spatial_relations = g.v().has_label("Place").as_("a") \
            .out("used_locations").out("location") \
            .has_label("SpatialRelation") \
            .out("other_place").as_("b") \
            .select("a", "b")

        for error in filter(
                lambda d: d["a"] == d["b"],
                spatial_relations):
            node = error["a"]

            labels = dict(map(
                lambda d: (d["lang"], d["text"]),
                g.v().has_label("Place").limit(1).out("labels")
                .has("is_preferred", "true").value_map()))

            for lang in ("eng", "deu"):
                if lang in labels:
                    label = labels[lang]
            else:
                label = list(labels.values())[0]

            hhb_id = int(next(g.v(node).values("hhb_id")))

            LOG.error(
                "Place %s (%u) has a spatial relation with itself.",
                label, hhb_id)
            num_errors += 1

            del hhb_id, label

        return num_errors
