import abc

from typing import (Any, Dict, List, Optional, Type)

from normlib.revision import Revision
from normlib.db.sidecar_db import (SidecarDBBackend, SidecarDBConnection)


class RevisionManager(metaclass=abc.ABCMeta):
    def __new__(cls, sidecar_db: SidecarDBConnection,
                *args: Any, **kwargs: Any) -> "RevisionManager":
        """Creates a new RevisionManager based on the provided DB connection.

        :param sidecar_db: A SidecarDBConnection
        :type sidecar_db: normlib.db.sidecar_db.SidecarDBConnection
        :raises NotImplementedError: when the DB type is not (yet) supported
        :returns: RevisionManager -- a new RevisionManager instance
        """

        if cls is not RevisionManager:
            # __new__ of subclasses will fall through to this __new__ and
            # create infinite recursion
            return object.__new__(cls)  # type: ignore

        if not isinstance(sidecar_db, SidecarDBConnection):
            raise TypeError("sidecar_db must be a SidecarDBConnection")

        real_cls = None  # type: Optional[Type[RevisionManager]]
        if sidecar_db.backend is SidecarDBBackend.POSTGRESQL:
            from normlib.db.pgsql.revision_manager import PgSQLRevisionManager
            real_cls = PgSQLRevisionManager
        # elif state_db.backend is StateDBBackend.SQLITE:
        #     from .sqlite.revision_manager import SQLiteRevisionManager
        #     real_cls = SQLiteRevisionManager

        if real_cls is not None:
            return real_cls.__new__(real_cls, sidecar_db, *args, **kwargs)

        raise NotImplementedError("This DB dialect is not supported")

    def _dict_to_rev(self, rev_dict: Dict[str, Any]) -> Revision:
        """Creates a Revision object from a row stored in the DB.

        :param rev_dict: A dict consisting of DB columns
        :type rev_dict: Dict[str, Any]
        :returns: Revision -- the DB row wrapped in a Revision object.
        """

        return Revision(**rev_dict)

    def _rev_to_dict(self, rev: Revision) -> Dict[str, Any]:
        """Converts a Revision object to a dict that can be stored in the DB.

        :param rev: The Revision to be converted.
        :type rev: normlib.db.revision.Revision
        :returns: Dict[str, Any] -- the "raw" information
        """

        return dict(rev)

    @abc.abstractmethod
    def __init__(self, sidecar_db: SidecarDBConnection) -> None:
        if not isinstance(sidecar_db, SidecarDBConnection):
            raise TypeError("sidecar_db must be a SidecarDBConnection")
        self.db = sidecar_db

    @abc.abstractmethod
    def create_revision(self, message: str) -> Revision:
        """Creates a new revision in the database and returns its information
        wrapped in a Revision object.

        :param message: The message of the revision
        :type message: str
        :returns: Revision -- the created Revision
        """

    @abc.abstractmethod
    def list_revisions(self) -> List[Revision]:
        """Fetches a list of all available Revisions in the DB.

        :returns: List[Revision] -- the list of Revisions ordered by
                  revision ID (descending)
        """

    @abc.abstractmethod
    def latest_revision(self) -> Optional[Revision]:
        """Fetches the latest revision from the DB.
        This includes not commited revisions.

        :returns: Optional[Revision] -- the latest Revision or None if there
                  are no revisions.
        """

    @abc.abstractmethod
    def get_by_id(self, rev_id: int) -> Revision:
        """Fetches a Revision from the database by ID.

        :param rev_id: the ID to fetch
        :type ref_id: int
        :returns: Revision -- the Revision (when found)
        :raises ValueError: if no revision with `rev_id` exists
        """

    @abc.abstractmethod
    def store_revision(self, rev: Revision) -> None:
        """Stores an updated Revision object to the DB.
        This method will do nothing, if no Revision exists with a matching ID.

        :param rev: the revision to store.
        :type rev: normlib.revision.Revision
        """

    def commit_revision(self, rev: Revision) -> None:
        """Commits a given Revision and updates the DB accordingly.

        :param rev: the revision to commit.
        :type rev: normlib.revision.Revision
        :raises ValueError: if the revision does not have an ID. No exception
        is raised if the revision does have an ID but it is invalid (i.e. not
        in the DB).
        """
        if not rev.id:
            raise ValueError("Given revision has no ID")

        import datetime

        rev.commited_at = datetime.datetime.now()
        self.store_revision(rev)

    @abc.abstractmethod
    def rollback_revision(self, rev: Revision) -> None:
        """Rolls back the given Revision (deletes it and all of its log entries)
        and updates the DB accordingly.

        :param rev: the revision to commit.
        :type rev: normlib.revision.Revision
        :raises ValueError: if the revision does not have an ID. No exception
        is raised if the revision does have an ID but it is invalid (i.e. not
        in the DB).
        """
