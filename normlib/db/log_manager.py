import abc

from typing import (
    Any, Dict, Generator, Iterable, List, Optional, Tuple, Type)

from normlib.change import (Action, ChangedEntity)
from normlib.revision import Revision
from normlib.db.revision_manager import RevisionManager
from normlib.db.sidecar_db import (SidecarDBBackend, SidecarDBConnection)


class LogManager(metaclass=abc.ABCMeta):
    def __new__(cls, sidecar_db: SidecarDBConnection,
                *args: Any, **kwargs: Any) -> "LogManager":
        """Creates a new LogManager based on the provided DB connection.

        :param sidecar_db: A SidecarDBConnection
        :type sidecar_db: normlib.db.sidecar_db.SidecarDBConnection
        :raises NotImplementedError: when the DB type is not (yet) supported
        :returns: LogManager -- a new LogManager instance
        """

        if cls is not LogManager:
            # __new__ of subclasses will fall through to this __new__ and
            # create infinite recursion
            return object.__new__(cls)  # type: ignore

        if not isinstance(sidecar_db, SidecarDBConnection):
            raise TypeError("sidecar_db must be a SidecarDBConnection")

        real_cls = None  # type: Optional[Type[LogManager]]
        if sidecar_db.backend is SidecarDBBackend.POSTGRESQL:
            from normlib.db.pgsql.log_manager import PgSQLLogManager
            real_cls = PgSQLLogManager
        # elif state_db.backend is StateDBBackend.SQLITE:
        #     from importmanager.db.sqlite.job_manager import SQLiteLogManager
        #     real_cls = SQLiteLogManager

        if real_cls is not None:
            return real_cls.__new__(real_cls, sidecar_db, *args, **kwargs)

        raise NotImplementedError("This DB dialect is not supported")

    def _dict_to_change(self, log_dict: Dict[str, Any]) -> ChangedEntity:
        """Creates a ChangedEntity object from a row stored in the DB.

        :param log_dict: A dict consisting of DB columns
        :type log_dict: Dict[str, Any]
        :returns: ChangedEntity -- the DB row wrapped in a ChangedEntity object
        """

        def _argify(log_dict: Dict[str, Any]) \
                -> Generator[Tuple[str, Any], None, None]:
            for k, v in log_dict.items():
                if k == "revision_id":
                    k = "revision"
                    v = self.revision_manager.get_by_id(v)
                elif k == "action":
                    v = Action(v)

                yield (k, v)

        args = dict(_argify(log_dict))
        return ChangedEntity(**args)

    def _change_to_dict(self, change: ChangedEntity) -> Dict[str, Any]:
        """Converts a ChangedEntity object to a dict that can be stored in the
        DB.

        :param change: The ChangedEntity to be converted.
        :type change: normlib.db.change.ChangedEntity
        :returns: Dict[str, Any] -- the "raw" information
        """

        def _conv(change: ChangedEntity) \
                -> Generator[Tuple[str, object], None, None]:
            for k, v in change:
                if k == "revision":
                    k = "revision_id"
                    v = v.id
                elif isinstance(v, Action):
                    v = v.value

                yield (k, v)

        return dict(_conv(change))

    @abc.abstractmethod
    def __init__(self, sidecar_db: SidecarDBConnection) -> None:
        if not isinstance(sidecar_db, SidecarDBConnection):
            raise TypeError("sidecar_db must be a SidecarDBConnection")
        self.db = sidecar_db
        self.revision_manager = RevisionManager(self.db)  # type: ignore

    @abc.abstractmethod
    def show_revision(self, revision: Revision) -> List[ChangedEntity]:
        """Fetches a list of changed entities for a Revision from the DB.

        :param rev: the revision to "show".
        :type rev: normlib.revision.Revision
        :returns: List[ChangedEntity] -- the list changed entities by the given
                  revision.
        """

    @abc.abstractmethod
    def changed_entities_since(self, revision: Revision) -> Tuple[int, ...]:
        """Fetches the histHub IDs of the entities which have been changed since
        (excluding) the given revision.

        :param rev: the "anchor" revision
        :type rev: normlib.revision.Revision
        :returns: Tuple[int] -- the histHub IDs of the entities which have been
                  changed since the given revision
        """

    def checkin_change(self, change: ChangedEntity) -> None:
        """Checks in a new change (add an entry to the log table).

        :param change: A ChangedEntity object.
        :type change: normlib.change.ChangedEntity
        """
        self.checkin_changes((change,))

    @abc.abstractmethod
    def checkin_changes(self, changes: Iterable[ChangedEntity]) -> None:
        """Checks in multiple changes (inserts multiple entries in the log
        table)

        :param changes: The ChangedEntity objects to check in.
        :type changes: Iterable[normlib.change.ChangedEntity]
        """
