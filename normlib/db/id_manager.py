import abc

from typing import TYPE_CHECKING

from normlib import HistHubID
from normlib.db.sidecar_db import (SidecarDBBackend, SidecarDBConnection)

if TYPE_CHECKING:
    from typing import (Any, Iterator, Optional, Type)


class HistHubIDManager(metaclass=abc.ABCMeta):
    def __new__(cls, sidecar_db: "SidecarDBConnection",
                *args: "Any", **kwargs: "Any") -> "HistHubIDManager":
        """Creates a new HistHubIDManager based on the provided DB connection.

        :param sidecar_db: A SidecarDBConnection
        :type sidecar_db: normlib.db.sidecar_db.SidecarDBConnection
        :raises NotImplementedError: when the DB type is not (yet) supported
        :returns: HistHubIDManager -- a new HistHubIDManager instance
        """

        if cls is not HistHubIDManager:
            # __new__ of subclasses will fall through to this __new__ and
            # create infinite recursion
            return object.__new__(cls)  # type: ignore

        if not isinstance(sidecar_db, SidecarDBConnection):
            raise TypeError("sidecar_db must be a SidecarDBConnection")

        real_cls = None  # type: Optional[Type[HistHubIDManager]]
        if sidecar_db.backend is SidecarDBBackend.POSTGRESQL:
            from normlib.db.pgsql.id_manager import PgSQLHistHubIDManager
            real_cls = PgSQLHistHubIDManager
        # elif state_db.backend is StateDBBackend.SQLITE:
        #     from normlib.db.sqlite.id_manager import SQLiteHistHubIDManager
        #     real_cls = SQLiteHistHubIDManager

        if real_cls is not None:
            return real_cls.__new__(real_cls, sidecar_db, *args, **kwargs)

        raise NotImplementedError("This DB dialect is not supported")

    @abc.abstractmethod
    def __init__(self, sidecar_db: SidecarDBConnection) -> None:
        if not isinstance(sidecar_db, SidecarDBConnection):
            raise TypeError("sidecar_db must be a SidecarDBConnection")
        self.db = sidecar_db

    def __next__(self) -> HistHubID:
        return self.request_id()

    def request_id(self) -> HistHubID:
        """Requests a single histHub ID.

        :returns: HistHubID - a histHub ID
        """
        return next(self.request_ids(1))

    @abc.abstractmethod
    def request_ids(self, count: int) -> "Iterator[HistHubID]":
        """Requests multiple histHub IDs.

        :param count: the number of histHub IDs to request.
        :type count: int
        :raises ValueError: if count is less than 1.
        :returns: Iterator[HistHubID] - an iterator yielding as many histHub
        IDs as requested.
        """
