from collections import defaultdict
from eavlib.graph import Node
from psycopg2 import sql  # type: ignore
from psycopg2.extras import NamedTupleCursor  # type: ignore
from typing import TYPE_CHECKING

from normlib import HistHubID
from normlib.db.dump_manager import DumpManager
from normlib.dump import (DeepNode, VirtualDeepNode, DeepNodeSerialiser)
from normlib.revision import Revision

from eavlib.connection import PostgresEAVConnection

if TYPE_CHECKING:
    from typing import (Dict, Optional, Union)


class PgSQLDumpManager(DumpManager):
    def dump_object(self, node_id: int, max_depth: int = 20) \
            -> "Optional[DeepNode]":
        if max_depth < 0:
            raise ValueError("max_depth must be >= 0")
        if node_id is None:
            raise ValueError("node_id is None")
        if not isinstance(node_id, int):
            raise TypeError("node_id must be int")

        graph = self.norm_db.graph
        if not isinstance(graph.connection, PostgresEAVConnection):
            raise TypeError("PostgreSQL connection expected!")
        conn = graph.connection
        sidecar_db = self.norm_db.sidecar_db

        if not conn.schema:
            raise RuntimeError("connection is missing schema attribute")

        try:
            rootlabel = next(graph.traversal().v(node_id).label())
            dn = DeepNode(
                rootlabel.node, str(rootlabel), {}, defaultdict(list))
        except StopIteration:
            # No nodes with the given IDs exist, so result is empty
            return None

        nodemap = {dn.node.id: dn}  # type: Dict[int, DeepNode]

        with conn.cursor(cursor_factory=NamedTupleCursor) as cursor:
            cursor.execute(sql.SQL(
                """
                SELECT
                  "map".*,
                  "mr"."name" AS "relation_name",
                  "tme".name AS "target_label",
                  "mr".card_max
                FROM {SIDECAR_SCHEMA_NAME}.map_object_relations(%s, %s) "map"
                LEFT JOIN {DATA_SCHEMA_NAME}."model_relation" "mr"
                    ON "mr".id = "map".relation_id
                LEFT JOIN {DATA_SCHEMA_NAME}."object" "tobj"
                    ON "tobj".id = "map".target_id
                LEFT JOIN {DATA_SCHEMA_NAME}."model_entity" "tme"
                    ON "tme".id = "tobj".entity_id
                ORDER BY "map"."depth" ASC;
                """).format(
                    DATA_SCHEMA_NAME=sql.Identifier(conn.schema),
                    SIDECAR_SCHEMA_NAME=sql.Identifier(sidecar_db.schema),
                ), (dn.node.id, max_depth))

            for row in cursor.fetchall():
                # HACK: to make typing happy stay compatible with Python 3.4
                if TYPE_CHECKING:
                    succ = VirtualDeepNode(Node(0), "")  \
                        # type: Union[DeepNode, VirtualDeepNode]
                if row.cycle or row.terminal:
                    succ = VirtualDeepNode(
                        Node(row.target_id), row.target_label)
                elif row.target_id in nodemap:
                    succ = nodemap[row.target_id]
                else:
                    succ = DeepNode(
                        Node(row.target_id), row.target_label,
                        {}, defaultdict(list))
                    nodemap[row.target_id] = succ

                pred = nodemap[row.object_id]
                if row.card_max != 1:
                    pred.relations[row.relation_name].append(succ)
                else:
                    if not isinstance(pred.relations[row.relation_name], list):
                        raise RuntimeError(
                            "%u: multiple relations for singular %s found!" % (
                                row.object_id, row.relation_name))
                    pred.relations[row.relation_name] = succ

            cursor.execute(sql.SQL(
                """
                SELECT
                  "oa".object_id,
                  "ma"."name" AS "attribute_name",
                  "oa".value
                FROM {DATA_SCHEMA_NAME}."object_attribute" "oa"
                LEFT JOIN {DATA_SCHEMA_NAME}."model_attribute" "ma"
                    ON "ma".id = "oa".attribute_id
                WHERE "oa".object_id IN %s
                """).format(
                    DATA_SCHEMA_NAME=sql.Identifier(conn.schema)
                ), (tuple(
                    k for k, v in nodemap.items()
                    if not isinstance(v, VirtualDeepNode)),))
            for row in cursor.fetchall():
                try:
                    nodemap[row.object_id].attributes[row.attribute_name] = \
                        row.value
                except KeyError:
                    raise RuntimeError("virtual %u is real!" % (row.object_id))

        del nodemap
        return dn

    def store_dump(self, dump: DeepNode, revision: Revision) -> None:
        with self.norm_db.sidecar_db.cursor() as cursor:
            cursor.execute(
                sql.SQL("""
                INSERT INTO {SCHEMA_NAME}.dump
                (histhub_id, revision_id, entity_dump)
                VALUES (%s, %s, %s)
                """).format(
                    SCHEMA_NAME=sql.Identifier(
                        self.norm_db.sidecar_db.schema)),
                (
                    int(dump.attributes["hhb_id"]),
                    revision.id,
                    DeepNodeSerialiser.as_xml(dump, enable_hidden=True),
                ))

    def get_snapshot(self, hhb_id: HistHubID, rev: Revision) -> str:
        if not rev.id:
            raise ValueError("Revision %r has no ID." % rev)

        sidecar_db = self.norm_db.sidecar_db
        with sidecar_db.cursor() as cursor:
            cursor.execute(
                sql.SQL("""
                SELECT entity_dump
                FROM {SCHEMA_NAME}.dump
                WHERE histhub_id = %(hhb_id)s
                  AND revision_id <= %(rev_id)s
                ORDER BY revision_id DESC
                LIMIT 1;
                """).format(SCHEMA_NAME=sql.Identifier(sidecar_db.schema)),
                {"hhb_id": hhb_id, "rev_id": rev.id})
            row = cursor.fetchone()
            if not row:
                raise RuntimeError(
                    "No snapshot for urn:histhub:%u@r%u" % (hhb_id, rev.id))
            return str(row[0])
