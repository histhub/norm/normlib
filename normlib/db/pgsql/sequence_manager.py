import collections
import logging

from typing import TYPE_CHECKING

from normlib.db.sequence_manager import SequenceManager
from normlib.db.sidecar_db import (SidecarDBConnection)

if TYPE_CHECKING:
    import psycopg2  # type: ignore
    from typing import (Iterable, Union)

LOG = logging.getLogger(__name__)


class PgSQLSequenceManager(SequenceManager):
    SequenceConfig = collections.namedtuple("SequenceConfig", (
        ("data_type", "start_value", "min_value", "max_value", "increment",
         "cycles")))

    def __init__(self, sidecar_db: SidecarDBConnection, sequence_name: str,
                 debug: bool) -> None:
        super().__init__(sidecar_db, sequence_name, debug)

        self._conf = self._get_sequence_config()
        LOG.debug("Sequence configuration: %r", self._conf)

        if self._conf.data_type not in ("integer", "bigint"):
            raise RuntimeError(
                "This script does not know how to handle sequences with "
                "data type %s" % (self._conf.data_type))

    def _get_sequence_config(self) -> "PgSQLSequenceManager.SequenceConfig":
        import psycopg2.extras  # type: ignore
        with self.sidecar_db.cursor(
                cursor_factory=psycopg2.extras.DictCursor) as cursor:
            cursor.execute("""
            SELECT
              data_type, start_value,
              minimum_value AS "min_value", maximum_value AS "max_value",
              "increment", cycle_option::boolean AS "cycles"
            FROM information_schema."sequences"
            WHERE sequence_schema = %s
             AND sequence_name = %s;
            """, (self.sidecar_db.schema, self.sequence_name))
            if not cursor.rowcount:
                raise RuntimeError(
                    "Sequence '%s' does not exist" % self.sequence_name)
            return self.SequenceConfig(**cursor.fetchone())

    def current_value(self) -> int:
        from psycopg2 import sql
        with self.sidecar_db.cursor() as cursor:
            cursor.execute(sql.SQL("""
            SELECT "last_value", is_called
            FROM {SCHEMA}.{SEQUENCE}
            """).format(
                SCHEMA=sql.Identifier(self.sidecar_db.schema),
                SEQUENCE=sql.Identifier(self.sequence_name)))
            (last_value, called) = cursor.fetchone()

            if called:
                return int(last_value) + int(self._conf.increment)
            else:
                return int(last_value)

    def _quote_strings(
            self, strings: "Iterable[str]",
            context: "Union[psycopg2.extensions.connection, psycopg2.extensions.cursor]") -> str:  # noqa: E501
        from psycopg2.extensions import quote_ident  # type: ignore
        return ".".join(quote_ident(s, context) for s in strings)

    def _set_value(self, value: int) -> None:
        with self.sidecar_db.cursor() as cursor:
            seq_ident = self._quote_strings(
                (self.sidecar_db.schema, self.sequence_name), cursor)
            cursor.callproc("setval", (seq_ident, value, False))
