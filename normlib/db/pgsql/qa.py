import logging

from typing import TYPE_CHECKING

from normlib.connection import NormDBConnection
from normlib.db.qa import NormDBQA

if TYPE_CHECKING:
    import psycopg2  # type: ignore

    from normlib.db.qa import _Tabular

LOG = logging.getLogger(__name__)


def _make_tabular(cursor: "psycopg2.extensions.cursor") -> "_Tabular":
    """Convert cursor result to a cliff.lister.Lister compatible format.
    """
    cols = tuple(x[0] for x in cursor.description)
    return (cols, tuple(map(tuple, cursor)))


class PgSQLNormDBQA(NormDBQA):
    def __init__(self, norm_db: NormDBConnection) -> None:
        super().__init__(norm_db)

        import psycopg2
        import psycopg2.extras  # type: ignore
        import psycopg2.sql  # type: ignore
        self._psycopg2 = psycopg2

        # Define a custom wait callback here to allow the script to be aborted
        # by SIGINT.
        psycopg2.extensions.set_wait_callback(psycopg2.extras.wait_select)

    def _load_checker_function(self, conn: "psycopg2.extensions.connection",
                               name: str) -> None:
        import os
        import pkg_resources

        with conn.cursor() as cursor:
            cursor.execute(
                "SELECT exists(SELECT * FROM pg_proc WHERE proname = %s)",
                (name,))
            if cursor.fetchone()[0]:
                return

        resdir = "data/postgresql/qa/"
        res_path = os.path.join(resdir, name + ".sql")

        query = pkg_resources.resource_string(
            "normlib", res_path).decode("utf-8")

        _sql = self._psycopg2.sql
        with conn.cursor() as cursor:
            # XXX: Should conn.schema be used here??
            cursor.execute(_sql.SQL(
                "SET search_path TO {}, pg_temp;").format(
                    _sql.Identifier(conn.schema)))

            cursor.execute(query)

    def _exec_sql_check(self, funcname: str) -> "_Tabular":
        conn = self.norm_db.graph.connection
        model_id = self.norm_db.graph.model_id

        self._load_checker_function(conn, funcname)

        with conn.cursor() as cursor:
            _sql = self._psycopg2.sql
            cursor.execute(
                _sql.SQL("SELECT * FROM pg_temp.{}({});").format(
                    _sql.Identifier(funcname),
                    _sql.Placeholder()),
                (model_id,))

            return _make_tabular(cursor)
