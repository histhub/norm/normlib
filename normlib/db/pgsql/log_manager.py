import collections
import logging

from psycopg2 import sql  # type: ignore
from psycopg2.extras import (DictCursor, execute_values)  # type: ignore
from typing import (Any, Dict, Iterable, List, Tuple)

from normlib.change import ChangedEntity
from normlib.revision import Revision
from normlib.db.log_manager import LogManager
from normlib.db.sidecar_db import (SidecarDBBackend, SidecarDBConnection)

LOG = logging.getLogger(__name__)


class PgSQLLogManager(LogManager):
    def __init__(self, sidecar_db: SidecarDBConnection) -> None:
        if sidecar_db.backend is not SidecarDBBackend.POSTGRESQL:
            raise TypeError(
                "sidecar_db must be a PostgreSQL backed SidecarDBConnection")
        super().__init__(sidecar_db)

    def show_revision(self, revision: Revision) -> List[ChangedEntity]:
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(
                sql.SQL("""
                SELECT *
                FROM {SCHEMA_NAME}.log
                WHERE log.revision_id = %s
                """).format(SCHEMA_NAME=sql.Identifier(self.db.schema)),
                (revision.id,))
            return [self._dict_to_change(dict(row)) for row in cursor]

    def changed_entities_since(self, revision: Revision) -> Tuple[int, ...]:
        with self.db.cursor() as cursor:
            cursor.execute(
                sql.SQL("""
                SELECT log.histhub_id
                FROM {SCHEMA_NAME}.log
                WHERE log.revision_id > %s
                """).format(SCHEMA_NAME=sql.Identifier(self.db.schema)),
                (revision.id,))
            return tuple(int(row[0]) for row in cursor)

    def checkin_changes(self, changes: Iterable[ChangedEntity]) -> None:
        def _batch_insert(
                keys: Iterable[str], dicts: Iterable[Dict[str, Any]]) -> None:
            with self.db.cursor() as cursor:
                execute_values(
                    cursor,
                    sql.SQL("INSERT INTO {SCHEMA_NAME}.log ({COLS}) VALUES %s")
                    .format(
                        SCHEMA_NAME=sql.Identifier(self.db.schema),
                        COLS=sql.SQL(", ").join(map(sql.Identifier, keys))),
                    dicts,
                    template="(%s)" % ", ".join("%%(%s)s" % k for k in keys))

        groups = collections.defaultdict(list) \
            # type: Dict[Tuple[str, ...], List[Dict[str, Any]]]
        for props in map(self._change_to_dict, changes):
            groups[tuple(props.keys())].append(props)

        for keys, dicts in groups.items():
            _batch_insert(keys, dicts)

        self.db.commit()
