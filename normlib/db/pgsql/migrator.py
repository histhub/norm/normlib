import logging
import pkg_resources

from typing import TYPE_CHECKING

from normlib.db.migrator import (SidecarDBMigrator, MigrationError)
from normlib.db.sidecar_db import SidecarDBConnection

if TYPE_CHECKING:
    from typing import Callable
    from eavlib.db.url import URL


class PgSQLSidecarDBMigrator(SidecarDBMigrator):
    def __init__(self, url: "URL", data_schema: str, debug: bool) -> None:
        super().__init__(url, data_schema, debug)

        import psycopg2  # type: ignore
        import psycopg2.extras  # type: ignore
        import psycopg2.sql  # type: ignore
        self._psycopg2 = psycopg2

        self.sidecar_db = SidecarDBConnection(url)
        self.data_schema = data_schema

        # Define a custom wait callback here to allow the script to be aborted
        # by SIGINT.
        psycopg2.extensions.set_wait_callback(psycopg2.extras.wait_select)

        class PgNoticesWriter:
            def __init__(self, callback: "Callable[[str], None]") -> None:
                if not callable(callback):
                    raise ValueError("callback is not callable")
                self.callback = callback

            def append(self, msg: str) -> None:
                self.callback(msg)

        def log_msg(msg: str) -> None:
            self._call_hooks(self.Hooks.log_message, logging.INFO, msg)
        self.sidecar_db.connection.notices = PgNoticesWriter(log_msg)

        self.sidecar_db.connection.isolation_level = \
            psycopg2.extensions.ISOLATION_LEVEL_SERIALIZABLE

        with self.sidecar_db.cursor() as cursor:
            cursor.execute("SET default_with_oids = false;")

    def create(self) -> None:
        sql = self._psycopg2.sql
        schema_name = self.sidecar_db.schema
        with self.sidecar_db.cursor() as cursor:
            cursor.execute(sql.SQL("""
            CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.version (
                schema_version integer NOT NULL
            )""").format(SCHEMA_NAME=sql.Identifier(schema_name)))

            cursor.execute(sql.SQL(
                "SELECT schema_version FROM {SCHEMA_NAME}.version").format(
                    SCHEMA_NAME=sql.Identifier(schema_name)))
            has_current_version = cursor.fetchone() is not None

            # Only set the version to 0 if the version table is empty.
            # NOTE: The version table could be non-empty if create() is called
            # on a pre-initialised schema.
            if not has_current_version:
                cursor.execute(sql.SQL("""
                TRUNCATE TABLE {SCHEMA_NAME}.version;
                INSERT INTO {SCHEMA_NAME}.version (schema_version) VALUES (%s);
                """).format(SCHEMA_NAME=sql.Identifier(schema_name)), (0,))

        self.sidecar_db.commit()

    def create_schema(self) -> None:
        sql = self._psycopg2.sql
        with self.sidecar_db.cursor() as cursor:
            cursor.execute(sql.SQL(
                "CREATE SCHEMA IF NOT EXISTS {SCHEMA_NAME}").format(
                    SCHEMA_NAME=sql.Identifier(self.sidecar_db.schema)))
        self.sidecar_db.commit()

    def schema_exists(self) -> bool:
        with self.sidecar_db.cursor() as cursor:
            cursor.execute("""
            SELECT 1
            FROM information_schema.schemata
            WHERE schema_name = %s;
            """, (self.sidecar_db.schema,))
            return cursor.fetchone() is not None

    def is_created(self) -> bool:
        with self.sidecar_db.cursor() as cursor:
            cursor.execute("""
            SELECT 1
            FROM information_schema.tables
            WHERE table_schema = %s AND table_name = 'version';
            """, (self.sidecar_db.schema,))
            return cursor.fetchone() is not None

    def destroy(self) -> None:
        sql = self._psycopg2.sql
        with self.sidecar_db.cursor() as cursor:
            cursor.execute(sql.SQL("""
            DROP SCHEMA {SCHEMA_NAME} CASCADE;
            CREATE SCHEMA {SCHEMA_NAME};
            """).format(SCHEMA_NAME=sql.Identifier(self.sidecar_db.schema)))
        self.sidecar_db.commit()

    def current_version(self) -> int:
        sql = self._psycopg2.sql
        with self.sidecar_db.cursor() as cursor:
            cursor.execute(sql.SQL(
                "SELECT schema_version FROM {SCHEMA_NAME}.version").format(
                    SCHEMA_NAME=sql.Identifier(self.sidecar_db.schema)))
            return int(cursor.fetchone()[0])

    def run_migration(self, version: int) -> None:  # noqa: C901
        _sql = self._psycopg2.sql
        schema_name = self.sidecar_db.schema
        import psycopg2
        from psycopg2.extensions import (  # type: ignore
            ISOLATION_LEVEL_SERIALIZABLE as ISOL_SERIALIZABLE)

        connection = self.sidecar_db.connection
        num_scripts_executed = 0

        if connection.readonly:
            raise RuntimeError(
                "Cannot perform migration because connection is read-only")
        if connection.autocommit:
            raise RuntimeError(
                "Cannot perform migration because connection has auto-commit "
                "enabled.")

        self._call_hooks(self.Hooks.before_migration, version)
        for _ in range(5):  # up to 5 retries in case of serialization failure
            old_isolation_level = connection.isolation_level
            try:
                # Make sure we're on a fresh transaction before starting the
                # migration
                self.sidecar_db.commit()

                if old_isolation_level is not ISOL_SERIALIZABLE:
                    connection.isolation_level = ISOL_SERIALIZABLE

                # Find all migration scripts for this version and execute them
                # in alphabetical order
                for res_path in self._get_migration_resource_names(
                        "data/postgresql/migrations/", version):
                    with self.sidecar_db.cursor() as cursor:
                        sql = _sql.SQL(pkg_resources.resource_string(
                            "normlib", res_path).decode("utf-8"))
                        sql = sql.format(
                            DATA_SCHEMA_NAME=_sql.Identifier(self.data_schema),
                            SIDECAR_SCHEMA_NAME=_sql.Identifier(schema_name))
                        cursor.execute(sql)
                        del sql

                    num_scripts_executed += 1

                if not num_scripts_executed:
                    raise ValueError(
                        "No migration to version %u found." % version)

                # Bump schema version if all scripts have successfully applied
                with self.sidecar_db.cursor() as cursor:
                    cursor.execute(
                        _sql.SQL("""
                        TRUNCATE TABLE {SCHEMA_NAME}.version;
                        INSERT INTO {SCHEMA_NAME}.version (schema_version)
                        VALUES (%s);
                        """).format(
                            SCHEMA_NAME=_sql.Identifier(schema_name)),
                        (version,))
                self.sidecar_db.commit()
            except psycopg2.OperationalError as e:
                self.sidecar_db.rollback()
                if e.pgcode != psycopg2.errorcodes.SERIALIZATION_FAILURE:
                    raise
                # silent retry
            except Exception as e:
                self._call_hooks(self.Hooks.failed_migration, version, e)
                self.sidecar_db.rollback()
                if "res_path" in locals():
                    import os
                    raise MigrationError(
                        "%s: %s" % (os.path.basename(res_path), e)) from e
                else:
                    raise MigrationError(str(e)) from e
            else:
                break  # break to not trigger for's else
            finally:
                # Reset connection isolation level if needed
                if connection.isolation_level is not old_isolation_level:
                    connection.isolation_level = old_isolation_level
        else:
            raise RuntimeError("Maximum number of retries exhausted")

        self._call_hooks(
            self.Hooks.after_migration, version, num_scripts_executed)
