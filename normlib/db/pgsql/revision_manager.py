import logging

from collections import OrderedDict
from psycopg2 import sql  # type: ignore
from psycopg2.extras import DictCursor  # type: ignore
from typing import (List, Optional)

from normlib.revision import Revision
from normlib.db.revision_manager import RevisionManager
from normlib.db.sidecar_db import (SidecarDBBackend, SidecarDBConnection)

LOG = logging.getLogger(__name__)


class PgSQLRevisionManager(RevisionManager):
    def __init__(self, sidecar_db: SidecarDBConnection) -> None:
        if sidecar_db.backend is not SidecarDBBackend.POSTGRESQL:
            raise TypeError(
                "sidecar_db must be a PostgreSQL backed SidecarDBConnection")
        super().__init__(sidecar_db)

    def create_revision(self, message: str) -> Revision:
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(
                sql.SQL("""
                INSERT INTO {SCHEMA_NAME}.revision (message)
                VALUES (%s)
                RETURNING id, message, created_at
                """).format(SCHEMA_NAME=sql.Identifier(self.db.schema)), (
                    message,))
            (_id, _message, _created_at) = cursor.fetchone()
        self.db.commit()

        return Revision(message=_message, id=_id, created_at=_created_at)

    def list_revisions(self) -> List[Revision]:
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(
                sql.SQL("""
                SELECT * FROM {SCHEMA_NAME}.revision r
                ORDER BY r.id DESC
                """).format(SCHEMA_NAME=sql.Identifier(self.db.schema)))
            return [self._dict_to_rev(dict(rev)) for rev in cursor]

    def latest_revision(self) -> Optional[Revision]:
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(
                sql.SQL("""
                SELECT * FROM {SCHEMA_NAME}.revision r
                ORDER BY r.id DESC
                LIMIT 1
                """).format(SCHEMA_NAME=sql.Identifier(self.db.schema)))

            row = cursor.fetchone()
            return self._dict_to_rev(dict(row)) if row is not None else None

    def get_by_id(self, rev_id: int) -> Revision:
        with self.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(
                sql.SQL("""
                SELECT *
                FROM {SCHEMA_NAME}.revision
                WHERE id = %s
                """).format(SCHEMA_NAME=sql.Identifier(self.db.schema)),
                (rev_id,))
            row = cursor.fetchone()

        if not row:
            raise ValueError(
                "There exists no revision with ID %u" % rev_id)

        return self._dict_to_rev(dict(row))

    def store_revision(self, rev: Revision) -> None:
        if not isinstance(rev, Revision):
            raise TypeError("rev to store must be a Revision")
        if not rev.id:
            raise ValueError("Given revision has no ID.")

        props = OrderedDict(filter(
            lambda item: item[0] != "id", self._rev_to_dict(rev).items()))

        LOG.debug("Storing properties for revision id %u: %s" % (
            rev.id, props))

        query = sql.SQL("""
        UPDATE {SCHEMA_NAME}.revision
        SET ({PROPS}) = %s
        WHERE id = %s""").format(
            SCHEMA_NAME=sql.Identifier(self.db.schema),
            PROPS=sql.SQL(", ").join(map(sql.Identifier, props)))
        params = (tuple(props.values()), rev.id)

        with self.db.cursor() as cursor:
            cursor.execute(query, params)
        self.db.commit()

    def rollback_revision(self, rev: Revision) -> None:
        if not isinstance(rev, Revision):
            raise TypeError("rev to store must be a Revision")
        if not rev.id:
            raise ValueError("Given revision has no ID.")

        # Rollback (potentially) aborted transaction to allow for a
        # rollback.
        self.db.rollback()

        with self.db.cursor() as cursor:
            cursor.execute(
                sql.SQL("""
                DELETE FROM {SCHEMA_NAME}.log
                WHERE revision_id = %s""").format(
                    SCHEMA_NAME=sql.Identifier(self.db.schema)),
                (rev.id,))
            cursor.execute(
                sql.SQL("""
                DELETE FROM {SCHEMA_NAME}.revision WHERE id = %s""").format(
                    SCHEMA_NAME=sql.Identifier(self.db.schema)),
                (rev.id,))

        # NOTE[dc]: The id property of rev is not unset, because I consider
        #           Revision objects immutable. This should not be a problem,
        #           because the ID should stay unique even after the revision
        #           is rolled back, as new revision IDs are supposed to be
        #           generated using the database sequence.
        #           If the user alters the sequence or defines his own IDs,
        #           he's asked for trouble anyway.

        self.db.commit()
