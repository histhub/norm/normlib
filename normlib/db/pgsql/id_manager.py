import psycopg2  # type: ignore

from psycopg2 import sql
from typing import TYPE_CHECKING

from normlib import HistHubID
from normlib.db.id_manager import HistHubIDManager
from normlib.db.sidecar_db import (SidecarDBBackend, SidecarDBConnection)

if TYPE_CHECKING:
    from typing import Iterator


class PgSQLHistHubIDManager(HistHubIDManager):
    def __init__(self, sidecar_db: SidecarDBConnection) -> None:
        super().__init__(sidecar_db)
        if sidecar_db.backend is not SidecarDBBackend.POSTGRESQL:
            raise TypeError(
                "sidecar_db must be a PostgreSQL backed SidecarDBConnection")

    def request_ids(self, count: int) -> "Iterator[HistHubID]":
        try:
            with self.db.cursor() as cursor:
                cursor.execute(
                    sql.SQL("""
                    SELECT {SCHEMA_NAME}.request_histhub_ids(%s);
                    """).format(SCHEMA_NAME=sql.Identifier(self.db.schema)),
                    (count,))
                yield from (HistHubID(row[0]) for row in cursor)
        except psycopg2.InternalError as e:
            self.db.rollback()
            if e.pgcode == psycopg2.errorcodes.RAISE_EXCEPTION:
                raise Exception(e.diag.message_primary) from e
            else:
                raise
