from typing import TYPE_CHECKING

from psycopg2.extras import DictCursor  # type: ignore

from normlib.change import ChangedEntity
from normlib.db.entity_manager import EntityManager
from normlib.db.log_manager import LogManager
from normlib.db.sidecar_db import SidecarDBBackend

if TYPE_CHECKING:
    from normlib import HistHubID
    from normlib.connection import NormDBConnection


class PgSQLEntityManager(EntityManager):
    def __init__(self, norm_db: "NormDBConnection") -> None:
        if norm_db.sidecar_db.backend is not SidecarDBBackend.POSTGRESQL:
            raise TypeError(
                "norm_db must be a PostgreSQL backed NormDBConnection")
        super().__init__(norm_db)

    def last_change(self, hhb_id: "HistHubID") -> ChangedEntity:
        import psycopg2.sql as sql  # type: ignore

        sidecar_db = self.norm_db.sidecar_db
        log_manager = LogManager(sidecar_db)  # type: ignore

        with sidecar_db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(
                sql.SQL("""
                SELECT *
                FROM {SCHEMA_NAME}.log
                WHERE log.histhub_id = %s
                ORDER BY log.revision_id DESC
                LIMIT 1;
                """).format(SCHEMA_NAME=sql.Identifier(sidecar_db.schema)),
                (hhb_id,))
            return log_manager._dict_to_change(cursor.fetchone())
