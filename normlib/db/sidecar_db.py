import enum

from eavlib.db.url import URL
from typing import Any

SidecarDBBackend = enum.Enum("SidecarDBBackend", (
    "POSTGRESQL",
    # "SQLITE",
    ))


class SidecarDBConnection:
    def __init__(self, url: URL) -> None:
        """Creates a new SidecarDBConnection based on the provided URL.

        :param url: A database connection URL
        :raises NotImplementedError: when the DB dialect is not (yet) supported
        """

        self.url = url
        dialect = url.get_backend_name()

        if dialect == "postgresql":
            import psycopg2  # type: ignore
            self.backend = SidecarDBBackend.POSTGRESQL
            self.connection = psycopg2.connect(**url.translate_connect_args())
            self.connection.set_client_encoding("UTF8")
            if not url.schema:
                raise ValueError("URL has no schema name")
            self.schema = url.schema
        # elif dialect == "sqlite":
        #     import sqlite3
        #     self.connection = ...
        else:
            raise NotImplementedError(
                "DB dialect %s is not supported" % dialect)

    def copy(self) -> "SidecarDBConnection":
        return SidecarDBConnection(self.url)

    def cursor(self, *args: Any, **kwargs: Any) -> Any:
        return self.connection.cursor(*args, **kwargs)

    def commit(self, *args: Any, **kwargs: Any) -> Any:
        return self.connection.commit(*args, **kwargs)

    def rollback(self, *args: Any, **kwargs: Any) -> Any:
        return self.connection.rollback(*args, **kwargs)

    def close(self, *args: Any, **kwargs: Any) -> Any:
        return self.connection.close(*args, **kwargs)

    @property
    def closed(self) -> bool:
        return bool(self.connection.closed)
