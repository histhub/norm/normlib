import abc
import logging

from normlib.db.sidecar_db import (SidecarDBBackend, SidecarDBConnection)

LOG = logging.getLogger(__name__)


class SequenceManager(metaclass=abc.ABCMeta):
    def __new__(cls, sidecar_db: SidecarDBConnection,
                sequence_name: str, debug: bool = False) -> "SequenceManager":
        if cls is not SequenceManager:
            # __new__ of subclasses will fall through to this __new__ and
            # create infinite recursion
            return object.__new__(cls)  # type: ignore

        backend = sidecar_db.backend

        if backend is SidecarDBBackend.POSTGRESQL:
            from normlib.db.pgsql.sequence_manager import PgSQLSequenceManager
            return PgSQLSequenceManager(sidecar_db, sequence_name, debug)
        else:
            raise NotImplementedError(
                "SidecarDBBackend.%s is not supported" % (backend))

    def __init__(self, sidecar_db: SidecarDBConnection, sequence_name: str,
                 debug: bool) -> None:
        self.sidecar_db = sidecar_db
        self.sequence_name = sequence_name
        self.debug = debug

    @abc.abstractmethod
    def current_value(self) -> int:
        """Returns the current sequence value, i.e. the value that would be
        returned as the next sequence value.
        This is to guarantee that the method will always return a sensible
        value (even for brand-new sequences.)

        :returns: int -- the current sequence value
        """

    def set_value(self, value: int, force: bool = False) -> None:
        """Set the sequence value to `value`.

        :param value: the value to set.
        :type value: int
        :param force: set the value even if it's smaller than the current
        value.
        :type force: bool
        """
        current = self.current_value()

        if current > value and not force:
            LOG.error(
                "The current value %d is bigger than the value to set %d. "
                "Use --force to change anyway.", current, value)
            return
        if current == value:
            LOG.warning(
                "The value %d is equal to the current sequence value.", value)
            return

        LOG.debug("Current sequence value: %d", current)

        self._set_value(value)

        LOG.info("New sequence value: %d", self.current_value())

    @abc.abstractmethod
    def _set_value(self, value: int) -> None:
        """Set the sequence value to `value`.
        This method makes no checks, so it should only be used internally.

        :param value: the value to set.
        :type value: int
        """
