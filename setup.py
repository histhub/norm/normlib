#!/usr/bin/env python3

import codecs
import os

from setuptools import setup, find_packages

package_name = "normlib"
min_python_version = (3, 4)

base = os.path.abspath(os.path.dirname(__file__))

try:
    from pip._internal.req import parse_requirements
except ImportError:
    try:
        from pip.req import parse_requirements
    except ImportError:
        raise RuntimeError("Could not find pip requirements parser")


def read_file(path):
    """Read file and return contents"""
    try:
        with codecs.open(path, "r", encoding="utf-8") as f:
            return f.read()
    except IOError:
        return ""


def get_reqs(target):
    """Parse requirements.txt files and return array"""
    reqs = parse_requirements(
        os.path.join(base, "requirements.d", ("%s.txt" % target)),
        session="hack")
    return [
        r.requirement if hasattr(r, "requirement") else str(r.req)
        for r in reqs]


def get_meta(name):
    """Get metadata from project's __init__.py"""
    return getattr(__import__(package_name), name)


def dedup(seq):
    from collections import OrderedDict
    return list(OrderedDict.fromkeys(seq).keys())


setup(
    name=package_name,
    version=get_meta("__version__"),

    description=get_meta("__description__"),
    long_description=read_file(os.path.join(base, "README.rst")),

    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python",
        # TODO: Add missing classifiers
    ],

    install_requires=dedup(get_reqs("normlib") + get_reqs("bin")),

    packages=find_packages(),
    python_requires=(">=%s" % ".".join(map(str, min_python_version))),

    package_data={
        "normlib": ["py.typed", "data/*/*/*.sql"],
        },

    entry_points={
        "console_scripts": [
            "normdb = normlib.bin.normdb.main:main",
            ]
        })
