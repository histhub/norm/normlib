# normlib

normlib is an extension to [eavlib](https://gitlab.com/histhub/norm/eavlib.git).
It contains the histHub-specific parts of the histHub graph database (a.k.a. the
sidecar schema).
This includes the histHub ID system, revision control and tombstone redirection.

It also contains the `normdb` helper utility which can be used to format a new
histHub-compatible database, to migrate an existing one to a newer schema version
or to modify the histHub-ID counter.

e.g.
```console
# Format a new histHub-compatible database
$ normdb format postgresql://postgres@localhost/histhub --data-schema data --sidecar-schema sidecar
# or, migrate a database to a newer schema version
$ normdb migrate postgresql://postgres@localhost/histhub --data-schema data --sidecar-schema sidecar

# Set the histHub-ID counter to 10'000'000
$ normdb hhbid set postgresql://postgres@localhost/histhub.sidecar 10000000
```
**Note:** The database URLs are
[SQLAlchemy-like](https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.create_engine)
URL strings with extended `database.schema` notation.
